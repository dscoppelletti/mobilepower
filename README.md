Mobile Power
============

Framework for building Android Apps.

> This project is discontinued.

## Eclipse and ADT

Mobile Power is developed using [Eclipse](http://www.eclipse.org) with
[ADT](http://developer.android.com/tools/help/adt.html) (Android
Developer Tools).

In my opinion, ADT manages references to Android libraries in a
unconfortable way: you must insert the source code of **ALL** libraries
in your workspace as projects.
This is because of how ADT manages resources.

I haven't tried Android Studio yet since it is still in Beta release,
but I think it will not solve this problem: according to me, it would be
necessary to refactor how Android SDK deploys and looks up resources.

To conclude, if you clone the Mobile Power repository, you have to clone
too the following repositories from my GitHub space:

1. Android SDK

Then, you have to open **ALL** the projects in your workspace, and maybe
you can try to use working sets. Happy Hacking!

