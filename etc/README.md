
Third parties's software
========================

See the **README.md** files in each sub-directories in order to get the
needed third parties' software.

## Library references

You have to set some path variables in your workspace:

1. Start Eclipse.
2. Select the **Window** menu item and then click the **Preferences**
menu item to open the **Preferences** dialog.
3. Select the **General / Workspace / Linked Resources** node.
4. Set the **ANDROID_SDK** path variable to the absolute path of the
directory where you installed Android SDK (e.g. **/var/android/sdk**).
5. Set the **MOBILEPOWER_HOME** path variable to the absolute path of
the directory where you cloned the repository of Mobile Power (e.g.
**/var/mobilepower-android**).
6. Click the **OK** button to accept the **Preferences** dialog.

### Setting path variables

1. Click the **New** button to open the **New Variable** dialog.
2. Enter the name of the variable in the **Name** field.
3. Click the **Folder** button to open the **Folder selection** dialog.
4. Browse to select the desired folder.
5. Click the **OK** button to accept the **Folder selection** dialog and
set the folder in the **Location** field of the **New Variable** dialog.
6. Click the **OK** button to accept the **New Variable** dialog.

