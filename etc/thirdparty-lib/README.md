
Third parties' libraries
========================

**$MOBILEPOWER_HOME** = Directory where you installed Mobile Power (e.g.
**/var/mobilepower**).

## pom.xml

1. Download and install [Apache Maven](http://maven.apache.org).
2. Run the following command from
**$MOBILEPOWER_HOME/etc/thirdparty-lib**:

```
mvn
```

... in order to get the needed third parties' libraries.


