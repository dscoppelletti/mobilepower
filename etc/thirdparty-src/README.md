
Source code of the third parties' libraries
===========================================

**$MOBILEPOWER_HOME** = Directory where you installed Mobile Power (e.g.
**/var/mobilepower**).

## pom.xml

1. Download and install [Apache Maven](http://maven.apache.org).
2. Run the following command from
**$MOBILEPOWER_HOME/etc/thirdparty-src**:

```
mvn
```

... in order to get the source code of the third parties' libraries.

## .properties files

The **.properties** files attach the source code to the libraries, thus
you can get the Javadoc for the libraries in Eclipse ADT.


