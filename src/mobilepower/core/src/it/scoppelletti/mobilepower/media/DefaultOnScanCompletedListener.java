/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.media;

import android.media.*;
import android.net.*;
import org.slf4j.*;

/**
 * Gestore del completamento della scansione di un file multimediale.
 * 
 * @since 1.0
 */
public class DefaultOnScanCompletedListener implements
        MediaScannerConnection.OnScanCompletedListener {
    private static final Logger myLogger = LoggerFactory.getLogger(
            DefaultOnScanCompletedListener.class);
    
    /**
     * Costruttore.
     */
    public DefaultOnScanCompletedListener() {        
    }
    
    /**
     * Gestisce il completamento della scansione di un file.
     * 
     * @param path File scansionato.
     * @param uri  URI assegnato al file. Se la scansione fallisce, &egrave;
     *             {@code null}.
     */
    public void onScanCompleted(String path, Uri uri) {
        if (uri == null) {
            myLogger.error("Failed to scan media file {}.", path);
        } else {
            myLogger.info("Media file {} scanned to uri {}.", path, uri);
        }
    }
}
