/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.os;

import android.annotation.*;
import android.os.*;

/**
 * Classe di base dei processi asincroni.
 * 
 * @since 1.0
 */
public abstract class AbstractAsyncTask extends
        AsyncTask<Bundle, Integer, Bundle> implements AsyncTaskController {
    private final int myReqCode;
    private final AsyncTaskHost myHost;
    private long myStepCount;
    private long myCurrentStep;
    private int myStepScale;
    
    /**
     * Costruttore.
     * 
     * @param requestCode Codice della richiesta.
     * @param host        Ospite. Pu&ograve; essere {@code null}.
     */
    protected AbstractAsyncTask(int requestCode, AsyncTaskHost host) {
        myReqCode = requestCode;
        myHost = host;        
        myStepCount = 100;
        myStepScale = 1;
    }
    
    /**
     * Imposta il numero di passi del processo.
     * 
     * @param value Valore.
     */
    protected final void setStepCount(long value) {
        if (value <= 0) {
            throw new IllegalArgumentException(String.format(
                    "Argument value %1$d is less than 1.", value));
        }
        
        myStepCount = value;
        myStepScale = 1;        
        while (value > Integer.MAX_VALUE) {
            myStepScale++;
            value = value / ((long) myStepScale);
        }                       
    }
    
    /**
     * Restituisce il numero di passi del processo scalato ad un numero intero.
     * 
     * @return Valore.
     */
    public final int getScaledStepCount() {
        return (int) (myStepCount / ((long) myStepScale));
    }
    
    public final void incrementProgress() {
        int progress;
        
        myCurrentStep++;
        
        progress = (int) (myCurrentStep / ((long) myStepScale));
        publishProgress(progress);
    }
    
    /**
     * Inizio del processo.
     */
    @Override
    protected final void onPreExecute() {
        myCurrentStep = 0;
    }    
    
    /**
     * Esegue il processo.
     * 
     * @params params Parametri.
     * @return        Risultato.
     */
    protected final Bundle doInBackground(Bundle... params) {
        if (params == null || params.length == 0 || params[0] == null) {
            throw new NullPointerException("Argument params is null.");
        }
        
        return start(params[0]);
    }
    
    /**
     * Avvia il processo.
     * 
     * @params args Parametri.
     */    
    protected abstract Bundle start(Bundle params);
    
    /**
     * Termine del processo.
     * 
     * @param result Risultato;
     */
    @Override
    protected final void onPostExecute(Bundle result) {
        if (myHost != null) {
            myHost.onPostExecute(myReqCode, result, false);
        }
    }  
    
    /**
     * Annullamento del processo.
     */
    @Override
    protected final void onCancelled() {
        if (myHost != null) {
            myHost.onPostExecute(myReqCode, null, true);
        }        
    }  
    
    /**
     * Annullamento del processo.
     */
    @SuppressLint("Override")
    protected final void onCancelled(Bundle result) {
        if (myHost != null) {
            myHost.onPostExecute(myReqCode, result, true);
        }        
    }      
    
    /**
     * Avanzamento del processo.
     */
    @Override
    protected final void onProgressUpdate(Integer... progress) {
        if (myHost != null && progress != null && progress.length > 0) {
            myHost.onProgressUpdate(myReqCode, progress[0]);
        }
    }    
}
