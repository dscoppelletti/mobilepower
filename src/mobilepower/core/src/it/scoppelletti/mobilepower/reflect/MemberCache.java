/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.reflect;

import java.lang.ref.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * Cache dei membri delle classi.
 * 
 * @since 1.0
 */
public final class MemberCache {
    private final Object mySyncRoot;
    private SoftReference<Map<MemberCache.Key, SoftReference<Member>>> myMap;
    
    /**
     * Costruttore privato per classe singleton.
     */
    private MemberCache() {
        mySyncRoot = new Object();
        myMap = null;
    }
    
    /**
     * Restituisce l&rsquo;istanza.
     * @return
     */
    public static MemberCache getInstance() {
        return InstanceHolder.myInstance;
    }
    
    /**
     * Restituisce il membro di una classe.
     * 
     * @param  clazz      Classe.
     * @param  memberCode Codice del membro.
     * @param  resolver   Rilevatore dei membri delle classi.
     * @return            Membro.
     */
    public Member getMember(Class<?> clazz, int memberCode,
            MemberCache.Resolver resolver) {
        Member member;
        MemberCache.Key key;
        SoftReference<Member> memberRef;        
        Map<MemberCache.Key, SoftReference<Member>> map;
        
        if (clazz == null) {
            throw new NullPointerException("Argument clazz is null.");
        }
        if (resolver == null) {
            throw new NullPointerException("Argument resolver is null.");
        }
                
        synchronized (mySyncRoot) {
            key = new MemberCache.Key(clazz, memberCode);
            map = (myMap == null) ? null : myMap.get();            
            if (map != null) {
                memberRef = map.get(key);
                member = (memberRef == null) ? null : memberRef.get();
                if (member != null) {
                    return member;
                }                
            }
                       
            member = resolver.resolveMember(clazz, memberCode);
            if (member == null) {
                throw new NullPointerException(
                        "Method resolveMember returned null.");
            }
            if (map == null) {
                map = new HashMap<MemberCache.Key, SoftReference<Member>>();
                myMap = new SoftReference<Map<MemberCache.Key,
                        SoftReference<Member>>>(map);
            }
            
            memberRef = new SoftReference<Member>(member);            
            map.put(key, memberRef);
        }        
        
        return member;
    }
    
    /**
     * Rilevatore dei membri delle classi.
     * 
     * @since 1.0.0
     */
    public static interface Resolver {
        
        /**
         * Rileva il membro di una classe.
         * 
         * @param  clazz      Classe.
         * @param  memberCode Codice del membro.
         * @return            Membro. Non pu&ograve; essere {@code null}.
         */
        Member resolveMember(Class<?> clazz, int memberCode);
    }
    
    /**
     * Inizializzatore on-demand che non necessita di sincronizzazione.
     */    
    private static final class InstanceHolder {
        static final MemberCache myInstance = new MemberCache();        
    }
    
    /**
     * Chiave del cache {@code MemberCache}.
     */
    private static final class Key {
        private final Class<?> myClass;
        private final int myMemberCode;
        
        /**
         * Costruttore.
         * 
         * @param clazz      Classe.
         * @param memberCode Codice del membro.
         */
        Key(Class<?> clazz, int memberCode) {
            myClass = clazz;
            myMemberCode = memberCode; 
        }
        
        /**
         * Verifica l&rsquo;uguaglianza con un altro oggetto.
         * 
         * @param  obj Oggetto di confronto.
         * @return     Esito della verifica. 
         */
        @Override
        public boolean equals(Object obj) {
            MemberCache.Key op;
            
            if (!(obj instanceof MemberCache.Key)) {
                return false;
            }
            
            op = (MemberCache.Key) obj;
            
            if (!myClass.equals(op.myClass)) {
                return false;
            }
            
            if (myMemberCode != op.myMemberCode) {
                return false;
            }
            
            return true;
        }
        
        /**
         * Calcola il codice hash dell&rsquo;oggetto.
         * 
         * @return Valore.
         */
        @Override
        public int hashCode() {
            int value = 17;
            
            value = 37 * value + myClass.hashCode();
            value = 37 * value + myMemberCode;
            
            return value;
        }
    }   
}
