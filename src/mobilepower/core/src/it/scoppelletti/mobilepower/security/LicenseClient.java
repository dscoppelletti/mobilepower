/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.security;

import com.google.android.vending.licensing.*;
import org.slf4j.*;

/**
 * Client del servizio di verifica delle licenze.
 * 
 * @since 1.0
 */
public abstract class LicenseClient implements LicenseCheckerCallback {
    
    /**
     * Risultato positivo.
     */
    public static final int RESULT_POSITIVE = 1;
    
    /**
     * Risultato negativo.
     */
    public static final int RESULT_NEGATIVE = 2;
    
    /**
     * Risultato indeterminato (la verifica andrebbe ritentata).
     */
    public static final int RESULT_RETRY = 3;
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            LicenseClient.class);
    private LicenseChecker myService;
    
    /**
     * Costruttore.
     * 
     * @param licenseChecker Client del servizio di verifica delle licenze. 
     */
    protected LicenseClient(LicenseChecker licenseChecker) {
        if (licenseChecker == null) {
            throw new NullPointerException("Argument licenseChecker is null.");
        }
        
        myService = licenseChecker;
    }       
       
    /**
     * Rilascia le risorse.
     */
    public final void onDestroy() {
        if (myService != null) {
            myService.onDestroy();
            myService = null;
        }
    }

    /**
     * Esegue la verifica della licenza.
     */
    public void check() {
        if (myService == null) {
            throw new IllegalStateException(String.format(
                    "Object %1$s destroyed.", this));
        }
        
        myService.checkAccess(this);
    }

    /**
     * Gestisce il risultato della verifica di una licenza.
     *  
     * @param result Risultato.
     */    
    protected abstract void onLicenseResult(int result);
    
    /**
     * Gestisce il risultato positivo della verifica della licenza.
     * 
     * @param reason Risultato.
     */
    public final void allow(int reason) {
        myLogger.debug("reason={}", reason);
        onLicenseResult(LicenseClient.RESULT_POSITIVE);
    }

    /**
     * Gestisce il risultato negativo della verifica della licenza.
     * 
     * @param reason Risultato.
     */    
    public final void dontAllow(int reason) {
        myLogger.warn("reason={}.", reason);
        onLicenseResult((reason == Policy.RETRY) ? LicenseClient.RESULT_RETRY :
            LicenseClient.RESULT_NEGATIVE);
    }

    /**
     * Gestisce un errore del sistema.
     * 
     * @param errorCode Codice dell&rsquo;errore.
     */
    public final void applicationError(int errorCode) {
        throw new RuntimeException(String.format("Internal error %1$d.",
                errorCode));
    }   
}
