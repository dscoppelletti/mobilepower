/*
 * Copyright (C) 2008-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.types;

import android.text.*;

/**
 * Funzioni di utilit&agrave; sulle stringhe.
 * 
 * @since 1.0
 */
public final class StringTools {  
       
    /**
     * String vuota.
     * 
     * @since 2.0
     */
    public static final String EMPTY = "";
    
    /**
     * Costruttore privato per classe statica.
     */
    private StringTools() {
    }
    
    /**
     * Verifica se due stringhe sono uguali.
     * 
     * <P>Il metodo {@code equals} della classe statica {@code Strings}
     * pu&ograve; essere pi&ugrave; comodo da usare della coppia di metodi
     * {@code equals} e {@code equalsIgnoreCase} della classe {@code String}
     * quando l&rsquo;indicatore per ignorare o meno la differenza tra i
     * caratteri maiuscoli e minuscoli &egrave; un parametro variabile.</P>
     * 
     * @param s1         Prima stringa. Pu&ograve; essere {@code null}.
     * @param s2         Seconda stringa. Pu&ograve; essere {@code null}.
     * @param ignoreCase Indica se ignorare la differenza tra caratteri
     *                   maiuscoli e minuscoli.
     * @return           Esito della verifica.
     */
    public static boolean equals(String s1, String s2, boolean ignoreCase) {
        if (TextUtils.isEmpty(s1)) {
            return TextUtils.isEmpty(s2);
        }
        
        if (s2 == null) {
            s2 = StringTools.EMPTY;
        }
        
        if (ignoreCase) {
            return s1.equalsIgnoreCase(s2);
        }
        
        return s1.equals(s2);
    }    
}
