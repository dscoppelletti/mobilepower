/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.types;

import java.util.*;

/**
 * Funzioni di utilit&agrave; sul tempo.
 * 
 * @since 1.0
 */
public final class TimeTools {

    /**
     * Valore di un componente non noto di un istante. Il valore della costante
     * &egrave; <CODE>{@value}</CODE>.
     */
    public static final int NA = -1;
    
    private static final String ZONE_UTC = "UTC";
    
    /**
     * Costruttore privato per classe statica.
     */
    private TimeTools() {        
    }
    
    /**
     * Restituisce l&rsquo;istante corrente UTC.
     * 
     * @return Calendario.    
     */
    public static Calendar getUTCNow() {
        return Calendar.getInstance(TimeZone.getTimeZone(TimeTools.ZONE_UTC));
    }    
}
