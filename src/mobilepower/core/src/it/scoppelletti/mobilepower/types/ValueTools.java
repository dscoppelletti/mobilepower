/*
 * Copyright (C) 2010-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.types;

import java.util.*;

/**
 * Funzioni di utilit&agrave; sui tipi-valore.
 * 
 * @since 1.0
 */
public final class ValueTools {
    
    /**
     * UUID nullo.
     * 
     * <P>La sezione 4.1.7 delle specifiche RFC 4122 definisce il valore
     * {@code NIL} come un UUID con tutti i bit azzerati.</P>
     * 
     * @see <A HREF="http://www.ietf.org/rfc/rfc4122.txt" TARGET="_blank">RFC
     *      4122: A Universally Unique IDentifier (UUID) URN Namespace</A> 
     */    
    public static final UUID UUID_NIL = new UUID(0, 0);
    
    /**
     * Costruttore privato per classe statica.
     */
    private ValueTools() {        
    }
    
    /**
     * Verifica se un valore e' {@code null} oppure un valore vuoto.
     * 
     * @param  value Valore.
     * @return       Esito della verifica.
     */
    public static boolean isNullOrEmpty(ValueType value) {
        return (value == null || value.isEmpty());
    }
}
