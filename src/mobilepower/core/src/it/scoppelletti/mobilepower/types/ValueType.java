/*
 * Copyright (C) 2010 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.types;

import android.annotation.SuppressLint;
import android.os.*;

/**
 * Interfaccia dei <DFN>tipi-valore</DFN>.
 * 
 * @since 1.0
 */
@SuppressLint("ParcelCreator")
public interface ValueType extends Parcelable {
    
    /**
     * Rappresenta l&rsquo;oggetto con una stringa.
     * 
     * @return Stringa.
     */
    String toString();
    
    /**
     * Verifica l&rsquo;uguaglianza con un altro oggetto.
     * 
     * @param  obj Oggetto di confronto.
     * @return     Esito della verifica. 
     */
    boolean equals(Object obj);

    /**
     * Calcola il codice hash dell&rsquo;oggetto.
     * 
     * @return Valore.
     */
    int hashCode();
    
    /**
     * Verifica se il valore &egrave; vuoto.
     * 
     * @return Esito della verifica.
     */
    boolean isEmpty();
    
    /**
     * Rappresentazione parcellizzata dell&rsquo;oggetto.
     * 
     * @param out   Flusso di scrittura.
     * @param flags Modalit&agrave;.
     */
    void writeToParcel(Parcel out, int flags);
    
    /**
     * Descrive gli eventuali tipi di oggetti particolari contenuti nella
     * rappresentazione gestita a parcellizzazione.
     * 
     * @return Maschera degli identificatori dei tipi di oggetto.
     */
    int describeContents();    
}
