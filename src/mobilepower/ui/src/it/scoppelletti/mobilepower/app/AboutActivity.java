/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.pm.*;
import android.os.*;
import android.text.*;
import android.text.util.*;
import android.widget.*;
import org.slf4j.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Attivit&agrave; di visualizzazione delle informazioni
 * sull&rsquo;applicazione.
 * 
 * @since 1.0
 */
public final class AboutActivity extends Activity {

    /**
     * Metadato di applicazione
     * {@code it.scoppelletti.mobilepower.app.copyright}: Avviso di copyright.
     */
    public static final String METADATA_COPYRIGHT =
            "it.scoppelletti.mobilepower.app.copyright";
    
    /**
     * Metadato di applicazione
     * {@code it.scoppelletti.mobilepower.app.license}: Nota di licenza.
     */
    public static final String METADATA_LICENSE =
            "it.scoppelletti.mobilepower.app.license";
        
    private static final Logger myLogger = LoggerFactory.getLogger(
            AboutActivity.class);
    
    /**
     * Costruttore.
     */
    public AboutActivity() {        
    }
        
    /**
     * Creazione dell&rsquo;attivit&agrave;.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza.
     */    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TextView textControl;
        MarketTagHandler tagHandler;
        
        super.onCreate(savedInstanceState);
                
        setTheme(AppUtils.getActivityTheme());
        setContentView(R.layout.textview);
        
        textControl = (TextView) findViewById(R.id.txt_content);
        textControl.setKeyListener(null);
        textControl.setAutoLinkMask(Linkify.EMAIL_ADDRESSES |
                Linkify.WEB_URLS);
        
        tagHandler = new MarketTagHandler();
        textControl.setText(Html.fromHtml(buildText(), null, tagHandler));
        tagHandler.addLinks(textControl,
                AppUtils.getFullPackageName(this, false));
    }
    
    /**
     * Costruisce il testo da visualizzare.
     * 
     * @return Testo.
     */
    private String buildText() {
        String pkgName, value;
        StringBuilder buf;
        Bundle data;
        ApplicationInfo applInfo;
        PackageInfo pkgInfo;
        PackageManager pkgMgr;
        
        pkgName = getPackageName();
        pkgMgr = getPackageManager();

        try {
            applInfo = pkgMgr.getApplicationInfo(pkgName,
                    PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException ex) {
            myLogger.error("Failed to get ApplicationInfo.", ex);
            applInfo = getApplicationInfo();            
        }        
        
        try {
            pkgInfo = pkgMgr.getPackageInfo(pkgName, 0); 
        } catch (PackageManager.NameNotFoundException ex) {
            myLogger.error("Failed to get PackageInfo.", ex);
            pkgInfo = null;       
        }    
        
        buf = new StringBuilder();
        buf.append("<h1>");
        buf.append(pkgMgr.getApplicationLabel(applInfo));
        if (pkgInfo != null) {
            buf.append("<br />");
            buf.append(pkgInfo.versionName);
        }
        buf.append("</h1>");
        
        data = applInfo.metaData;
        if (data == null) {
            return buf.toString();
        }
        
        value = data.getString(AboutActivity.METADATA_COPYRIGHT);
        if (!TextUtils.isEmpty(value)) {
            buf.append("<p>");
            buf.append(value);
            buf.append("</p>");
        }
        
        value = data.getString(AboutActivity.METADATA_LICENSE);
        if (!TextUtils.isEmpty(value)) {
            buf.append(value);                    
        }
        
        value = data.getString(AppUtils.METADATA_FULLPACKAGE);
        if (!TextUtils.isEmpty(value)) {
            buf.append(getString(R.string.msg_demo));
        }
        
        return buf.toString();
    }    
}
