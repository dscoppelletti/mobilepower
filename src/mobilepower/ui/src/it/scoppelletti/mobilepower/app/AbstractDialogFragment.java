/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.os.*;
import android.text.*;
import org.slf4j.*;

/**
 * Classe di base di un frammento che rappresenta un dialogo.
 * 
 * @since 1.0
 */
public abstract class AbstractDialogFragment extends DialogFragment {
    private static final Logger myLogger = LoggerFactory.getLogger(
            AbstractDialogFragment.class);
    
    /**
     * Costruttore.
     */
    protected AbstractDialogFragment() {        
    }
    
    /**
     * Gestisce il completamento della creazione dell&rsquo;attivit&agrave;.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza del frammento.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {        
        String tag;
        Activity activity;
        ActivitySupport support;        
        
        super.onActivityCreated(savedInstanceState);
        
        activity = getActivity();
        if (!(activity instanceof ActivitySupport)) {
            myLogger.debug("Activity not implement interface ActivitySupport.");
            return;
        }
        
        tag = getTag();
        if (TextUtils.isEmpty(tag)) {
            myLogger.debug("Tag is not set.");
            return;
        }
        
        support = (ActivitySupport) activity;
        support.onDialogFragmentShow(tag);       
    }  

    /**
     * Gestisce la distruzione del dialogo.
     */
    @Override
    public void onDestroy() {
        String tag;
        Activity activity;
        ActivitySupport support;
        
        super.onDestroy();
        
        activity = getActivity();
        if (!(activity instanceof ActivitySupport)) {
            myLogger.debug("Activity not implement interface ActivitySupport.");
            return;
        }        
                
        tag = getTag();
        if (TextUtils.isEmpty(tag)) {
            myLogger.debug("Tag is not set.");
            return;
        }
        
        support = (ActivitySupport) activity;
        support.onDialogFragmentDismiss(tag);        
    }
    
    /**
     * Istanzia un costruttore di dialoghi.
     * 
     * @return Oggetto.
     */
    protected final AlertDialog.Builder newAlertDialogBuilder() {
        return new AlertDialog.Builder(getActivity(),
                AppUtils.getDialogTheme());                
    }
}
