/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.os.*;
import android.preference.*;

/**
 * Classe di base delle attivit&agrave; di gestione dei parametri di
 * configurazione.
 * 
 * @since 1.0
 */
public abstract class AbstractPreferenceActivity extends PreferenceActivity {

    /**
     * Costruttore.
     */
    protected AbstractPreferenceActivity() {        
    }
    
    /**
     * Creazione dell&rsquo;attivit&agrave;.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(AppUtils.getActivityTheme());
    }
}
