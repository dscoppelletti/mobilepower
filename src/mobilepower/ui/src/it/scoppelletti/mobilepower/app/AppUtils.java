/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.net.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.view.inputmethod.*;
import org.slf4j.*;

/**
 * Funzioni di utilit&agrave; sul modello delle applicazioni.
 * 
 * @since 1.0
 */
public final class AppUtils {

    /**
     * Metadato di applicazione <CODE>{@value}</CODE>: pacchetto della versione
     * completa dell&rsquo;App. 
     */
    public static final String METADATA_FULLPACKAGE =
            "it.scoppelletti.mobilepower.app.fullPackage";
   
    /**
     * Preferenza <CODE>{@value}</CODE>: Codice dell&rsquo;ultima versione
     * installata.
     */
    public static final String PREF_LASTVER =
            "it.scoppelletti.mobilepower.app.lastVer";
    
    private static final int Theme_Holo = 16973931;
    private static final int DLG_THEME_DEVICE_DEFAULT_DARK = 4;
    private static final Logger myLogger = LoggerFactory.getLogger(
            AppUtils.class);
    
    /**
     * Costruttore privato per classe statica.
     */
    private AppUtils() {        
    }
    
    /**
     * Restituisce il nome del pacchetto di un&rsquo;applicazione.
     * 
     * @param  ctx        Contesto.
     * @param  onlyIfDemo Indica se restituire il nome del pacchetto solo se
     *                    l&rsquo;applicazione &egrave; una versione di demo.
     * @return            Nome del pacchetto. Se il parametro {@code onlyIfDemo}
     *                    &egrave; impostato, pu&ograve; essere {@code null}.
     */
    static String getFullPackageName(Context ctx, boolean onlyIfDemo) {
        String pkgName, value;
        Bundle data;
        ApplicationInfo applInfo;
        PackageManager pkgMgr;
        
        if (ctx == null) {
            throw new NullPointerException("Argument ctx is null.");
        }
        
        pkgName = ctx.getPackageName();
        pkgMgr = ctx.getPackageManager();
        
        try {
            applInfo = pkgMgr.getApplicationInfo(pkgName,
                    PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException ex) {
            myLogger.error("Failed to get ApplicationInfo.", ex);
            applInfo = ctx.getApplicationInfo();            
        }        
        
        data = applInfo.metaData;
        value = (data != null) ?
                data.getString(AppUtils.METADATA_FULLPACKAGE) : null; 
        if (!TextUtils.isEmpty(value)) {
            pkgName = value;
        } else if (onlyIfDemo) {
            return null;
        }
        
        return pkgName;
    }
    
    /**
     * Istanzia l&rsquo;intento per l&rsquo;acquisto di un&rsquo;applicazione.
     * 
     * @param  ctx Contesto.
     * @return     Intento.
     */
    public static Intent newBuyIntent(Context ctx) {
        String pkgName;
        Intent intent;
        Uri.Builder uriBuilder;
        
        if (ctx == null) {
            throw new NullPointerException("Argument ctx is null.");
        }
        
        pkgName = AppUtils.getFullPackageName(ctx, false);
        
        uriBuilder = new Uri.Builder();
        uriBuilder.scheme("market")
            .authority("details")
            .appendQueryParameter("id", pkgName);
        intent = new Intent(Intent.ACTION_VIEW, uriBuilder.build());
        
        return intent;
    }  
    
    /**
     * Restituisce il tema da applicare ad un&rsquo;attivit&agrave;.
     * 
     * @return Valore.
     */    
    public static int getActivityTheme() {
        return AppUtils.Theme_Holo;            
    }
        
    /**
     * Restituisce il tema da applicare ad un dialogo.
     * 
     * @return Valore.
     */
    public static int getDialogTheme() {
        return AppUtils.DLG_THEME_DEVICE_DEFAULT_DARK;
    }
    
    /**
     * Chiude la tastiera virtuale.
     * 
     * @param activity Attivit&agrave;.
     */
    public static void hideSoftInput(Activity activity) {
        View view;
        InputMethodManager inputMgr;
        
        if (activity == null) {
            throw new NullPointerException("Argument activity is null.");
        }
                
        view = activity.getCurrentFocus();
        if (view == null) {
            return;
        }
        
        inputMgr = (InputMethodManager) view.getContext().getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }    
}
