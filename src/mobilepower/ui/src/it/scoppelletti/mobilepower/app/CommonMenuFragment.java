/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.*;
import android.text.*;
import android.view.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Voci comuni del men&ugrave;.
 * 
 * @since 1.0
 */
public final class CommonMenuFragment extends Fragment {

    /**
     * Tag del frammento.
     */    
    public static final String TAG = "CommonMenu";

    /**
     * Azione di avvio dell&rsquo;attivit&agrave; di gestione delle impostazioni
     * dell&rsquo;applicazione.
     */
    public static final String ACTION_SETTINGS =
            "it.scoppelletti.mobilepower.intent.action.SETTINGS";
    
    /**
     * Azione di visualizzazione della Guida dell&rsquo;applicazione.
     */
    public static final String ACTION_HELP =
            "it.scoppelletti.mobilepower.intent.action.HELP";
    
    /**
     * Azione di visualizzazione dell&rsquo;informazioni
     * sull&rsquo;applicazione.
     */
    public static final String ACTION_ABOUT =
            "it.scoppelletti.mobilepower.intent.action.ABOUT";
    
    /**
     * Costruttore.
     */
    public CommonMenuFragment() {
        setHasOptionsMenu(true);
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @return Frammento.
     */
    public static CommonMenuFragment newInstance() {
        return new CommonMenuFragment();
    }
    
    /**
     * Imposta la visibilit&agrave; di tutte le voci comuni del men&ugrave;.
     * 
     * @param menu    Men&ugrave;.
     * @param visible Indicatore di visibilit&agrave;.
     */
    public static void setMenuItemVisible(Menu menu, boolean visible) {
        MenuItem menuItem;
        
        menuItem = menu.findItem(R.id.cmd_buy);
        if (menuItem != null) {
            menuItem.setVisible(visible);
        }
        
        menuItem = menu.findItem(R.id.cmd_settings);
        menuItem.setVisible(visible);
        menuItem = menu.findItem(R.id.cmd_help);
        menuItem.setVisible(visible);        
        menuItem = menu.findItem(R.id.cmd_about);
        menuItem.setVisible(visible);        
    }
    
    /**
     * Creazione del men&ugrave;.
     * 
     * @param menu     Men&ugrave;
     * @param inflater Parser del men&ugrave;
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        String pkgName;
       
        pkgName = AppUtils.getFullPackageName(getActivity(), true);
        
        if (TextUtils.isEmpty(pkgName)) {
            inflater.inflate(R.menu.common, menu);
        } else {
            inflater.inflate(R.menu.demo, menu);           
        }         
    }
    
    /**
     * Gestione della selezione di una voce di men&ugrave;.
     * 
     * @param  item Voce di men&ugrave;
     * @return      Indicatore di evento gestito.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {        
        int itemId = item.getItemId();
        String action = null;
        Intent intent = null;
        Context ctx = getActivity();
        
        if (itemId == R.id.cmd_settings) {
            action = CommonMenuFragment.ACTION_SETTINGS;
        } else if (itemId == R.id.cmd_help) {
            action = CommonMenuFragment.ACTION_HELP;            
        } else if (itemId == R.id.cmd_about) {
            action = CommonMenuFragment.ACTION_ABOUT;
        } else if (itemId == R.id.cmd_buy) {
            intent = AppUtils.newBuyIntent(ctx);
        }
        if (action != null) {
            intent = new Intent(action);
            intent.setPackage(ctx.getPackageName());
        }
        if (intent != null) {
            ctx.startActivity(intent);
            return true;            
        }
        
        return super.onOptionsItemSelected(item);
    }
}
