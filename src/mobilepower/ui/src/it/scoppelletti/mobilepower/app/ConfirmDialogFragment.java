/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.*;
import android.os.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Dialogo di conferma.
 * 
 * @since 1.0
 */
public final class ConfirmDialogFragment extends AbstractDialogFragment
    implements DialogInterface.OnClickListener {

    /**
     * Tag del frammento.
     */
    public static final String TAG = "ConfirmDialog";
    
    private static final String ARG_TITLEID = "titleId";
    private static final String ARG_MSGID = "msgId";    
    private DialogInterface.OnClickListener myOnClickListener;
    
    /**
     * Costruttore.
     */
    public ConfirmDialogFragment() {        
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @param  titleId Identificatore della risorsa del titolo.
     * @param  msgId   Identificatore della risorsa del messaggio.
     * @return         Frammento.
     */    
    public static ConfirmDialogFragment newInstance(int titleId, int msgId) {
        Bundle args;
        ConfirmDialogFragment fragment;
                
        args = new Bundle();
        args.putInt(ConfirmDialogFragment.ARG_TITLEID, titleId);
        args.putInt(ConfirmDialogFragment.ARG_MSGID, msgId);
                       
        fragment = new ConfirmDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    /**
     * Imposta il gestore della chiusura del dialogo.
     * 
     * @param obj Oggetto.
     */
    public void setOnClickListener(DialogInterface.OnClickListener obj) {
        myOnClickListener = obj;
    }

    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int resId;
        AlertDialog.Builder builder;
        Bundle args = getArguments();
        
        builder = newAlertDialogBuilder();
        
        resId = args.getInt(ConfirmDialogFragment.ARG_TITLEID, 0);
        if (resId > 0) {
            builder.setTitle(resId);
        }
        
        resId = args.getInt(ConfirmDialogFragment.ARG_MSGID, 0);
        if (resId > 0) {
            builder.setMessage(resId);
        }
        
        setCancelable(false);
        builder.setCancelable(false);
        
        builder.setPositiveButton(R.string.cmd_yes, this);        
        builder.setNegativeButton(R.string.cmd_no, this);
        
        return builder.create();        
    }
    
    /**
     * Gestisce la chiusura del dialogo.
     * 
     * @param dialog Dialogo.
     * @param which  Identificatore del bottone.
     */    
    public void onClick(DialogInterface dialog, int which) {
        if (myOnClickListener != null) {
            myOnClickListener.onClick(dialog, which);
        }
    }    
}
