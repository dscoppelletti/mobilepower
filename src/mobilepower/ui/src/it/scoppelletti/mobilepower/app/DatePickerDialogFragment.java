/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.os.*;
import android.widget.*;
import it.scoppelletti.mobilepower.types.*;

/**
 * Dialogo per impostazione di una data.
 * 
 * @since 1.0
 */
public final class DatePickerDialogFragment extends AbstractDialogFragment
        implements DatePickerDialog.OnDateSetListener {    
    private static final String ARG_VALUE = "value";
    private DatePickerDialog.OnDateSetListener myOnDateSetListener;
    
    /**
     * Costruttore.
     */
    public DatePickerDialogFragment() {        
    }

    /**
     * Istanzia un frammento.
     * 
     * @param  value Data.
     * @return       Frammento.
     */
    public static DatePickerDialogFragment newInstance(SimpleDate value) {
        Bundle args;
        DatePickerDialogFragment fragment;
               
        args = new Bundle();        
        if (!ValueTools.isNullOrEmpty(value)) {
            args.putParcelable(DatePickerDialogFragment.ARG_VALUE, value);
        }
        
        fragment = new DatePickerDialogFragment();
        fragment.setArguments(args);
        return fragment;        
    }

    /**
     * Imposta il gestore dell&rsquo;impostazione della data.
     * 
     * @param obj Oggetto.
     */
    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener obj) {
        myOnDateSetListener = obj;
    }
    
    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        SimpleDate value;
        DatePickerDialog dlg;
        Bundle args = getArguments();
        
        value = (SimpleDate) args.getParcelable(
                DatePickerDialogFragment.ARG_VALUE);
        if (ValueTools.isNullOrEmpty(value)) {
            value = SimpleDate.getToday();
        }
                        
        dlg = new DatePickerDialog(getActivity(), AppUtils.getDialogTheme(),
                this, value.getYear(), value.getMonth(), value.getDay());
        return dlg;
    }
    
    /**
     * Gestisce l&rsquo;impostazione della data.
     * 
     * @param view  Controllo.
     * @param year  Anno.
     * @param month Mese.
     * @param day   Giorno.
     */
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (myOnDateSetListener != null) {
            myOnDateSetListener.onDateSet(view, year, month, day);
        }
    }    
}
