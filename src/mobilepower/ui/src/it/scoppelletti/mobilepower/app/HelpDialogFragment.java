/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Dialogo di Guida.
 * 
 * @since 1.0
 */
public final class HelpDialogFragment extends AbstractDialogFragment implements
        DialogInterface.OnClickListener {

    /**
     * Tag del frammento.
     */
    public static final String TAG = "HelpDialog";

    /**
     * Modalit&agrave; di Guida. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */
    public static final int MODE_HELP = 0;
    
    /**
     * Modalit&agrave; di conferma. Il valore della costante &egrave;
     * <CODE>{@value}</CODE>.
     */    
    public static final int MODE_CONFIRM = 1;
            
    private static final String ARG_MODE = "mode";
    private static final String ARG_TITLEID = "titleId";
    private static final String ARG_TEXTID = "textId";
    private static final String ARG_PREFKEY = "prefKey";    
    
    private DialogInterface.OnClickListener myOnClickListener;
    
    /**
     * Costruttore.
     */
    public HelpDialogFragment() {        
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @param  mode    Modalit&agrave;.
     * @param  titleId Identificatore della risorsa del titolo.
     * @param  textId  Identificatore della risorsa del testo.
     * @param  prefKey Chiave della preferenza che indica se la Guida &egrave;
     *                 gi&agrave; stata visualizzata.
     * @return         Frammento.
     */
    public static HelpDialogFragment newInstance(int mode, int titleId,
            int textId, String prefKey) {
        Bundle args;
        HelpDialogFragment fragment;
        
        if (TextUtils.isEmpty(prefKey)) {
            throw new NullPointerException("Argument prefKey is null.");
        }
        
        args = new Bundle();
        args.putInt(HelpDialogFragment.ARG_MODE, mode);
        args.putInt(HelpDialogFragment.ARG_TITLEID, titleId);
        args.putInt(HelpDialogFragment.ARG_TEXTID, textId);
        args.putString(HelpDialogFragment.ARG_PREFKEY, prefKey);
                
        fragment = new HelpDialogFragment();
        fragment.setArguments(args);
        
        return fragment;
    }
    
    /**
     * Verifica se un&rsquo;attivit&agrave; ha gi&agrave; visualizzato una
     * Guida.
     * 
     * @param  activity Attivit&agrave;.
     * @param  prefKey  Chiave della preferenza che indica se la Guida &egrave;
     *                  gi&agrave; stata visualizzata.
     * @return          Esito della verifica.
     */
    public static boolean isShown(Activity activity, String prefKey) {
        SharedPreferences prefs;
        
        if (activity == null) {
            throw new NullPointerException("Argument activity is null.");
        }
        if (TextUtils.isEmpty(prefKey)) {
            throw new NullPointerException("Argument prefKey is null.");
        }
        
        prefs = activity.getPreferences(Context.MODE_PRIVATE);
        return prefs.getBoolean(prefKey, false);
    }
    
    /**
     * Imposta il gestore della chiusura del dialogo.
     * 
     * @param obj Oggetto.
     */
    public void setOnClickListener(DialogInterface.OnClickListener obj) {
        myOnClickListener = obj;
    }

    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int mode, resId;
        View contentView;
        AlertDialog dlg;
        AlertDialog.Builder builder;
        LayoutInflater inflater;
        TextView textControl;
        Bundle args = getArguments();
        
        builder = newAlertDialogBuilder();
        
        resId = args.getInt(HelpDialogFragment.ARG_TITLEID, 0);
        if (resId > 0) {
            builder.setTitle(resId);
        }
                
        inflater = getActivity().getLayoutInflater();
        contentView = inflater.inflate(R.layout.helpdialog, null);
        builder.setView(contentView);
        
        builder.setIcon(android.R.drawable.ic_dialog_info);
        
        mode = getArguments().getInt(HelpDialogFragment.ARG_MODE,
                HelpDialogFragment.MODE_HELP);
        switch (mode) {
        case HelpDialogFragment.MODE_CONFIRM:
            setCancelable(false);
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.cmd_proceed, this);        
            builder.setNegativeButton(android.R.string.cancel, this);            
            break;
            
        default: // HelpDialogFragment.MODE_HELP
            builder.setNeutralButton(R.string.cmd_close, null);
            break;
        }
        
        dlg = builder.create();
                
        textControl = (TextView) contentView.findViewById(R.id.txt_help);
        textControl.setKeyListener(null);
        
        resId = args.getInt(HelpDialogFragment.ARG_TEXTID, 0);
        if (resId > 0) {
            textControl.setText(Html.fromHtml(getResources().getString(resId)));
        }
        
        return dlg;        
    }    

    /**
     * Gestisce la chiusura del dialogo.
     * 
     * @param dialog Dialogo.
     * @param which  Identificatore del bottone.
     */    
    public void onClick(DialogInterface dialog, int which) {
        if (myOnClickListener != null) {
            myOnClickListener.onClick(dialog, which);
        }
    }
    
    /**
     * Gestisce la chiusura del dialogo.
     * 
     * @param dialog Dialogo.
     */
    @Override
    public void onDismiss(DialogInterface dialog) {
        String prefKey;
        SharedPreferences prefs;
        SharedPreferences.Editor editor;
     
        prefKey = getArguments().getString(HelpDialogFragment.ARG_PREFKEY);
        if (!TextUtils.isEmpty(prefKey)) {
            prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
            editor = prefs.edit();
        
            editor.putBoolean(prefKey, true);
            editor.apply();
        }
        
        super.onDismiss(dialog);
    }        
}
