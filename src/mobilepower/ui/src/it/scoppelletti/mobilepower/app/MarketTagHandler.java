/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import java.util.regex.*;
import android.text.*;
import android.text.util.*;
import android.widget.*;
import org.xml.sax.*;

/**
 * Gestore del tag <CODE>&lt;market&gt;</CODE>.
 */
final class MarketTagHandler extends Editable.Factory implements
        Html.TagHandler, Linkify.TransformFilter, Linkify.MatchFilter {
    private static final String SCHEME = "market://details?id=";
    private static final String TAG = "market";
    private int myStartPos;
    private int myEndPos;
    private String myPkgName;
    
    /**
     * Costruttore.
     */
    MarketTagHandler() {
        myStartPos = -1;
        myEndPos = -1;
    }
        
    /**
     * Gestisce un tag.
     * 
     * @param opening   Indicatore di tag aperto.
     * @param tag       Tag.
     * @param output    Testo di output.
     * @param xmlReader Flusso di lettura.
     */
    public void handleTag(boolean opening, String tag, Editable output,
            XMLReader xmlReader) {
        if (!MarketTagHandler.TAG.equalsIgnoreCase(tag)) {
            return;
        }
        
        if (opening) {
            if (myStartPos < 0) {
                myStartPos = output.length();
            }
        } else {
            if (myStartPos >= 0 && myEndPos < 0) {
                myEndPos = output.length();
            }
        }
    }

    /**
     * Attiva la rilevazione dei collegamenti.
     * 
     * @param view    Controllo.
     * @param pkgName Nome del pacchetto.
     */
    public void addLinks(TextView view, String pkgName) {
        String text;
        
        if (myEndPos <= myStartPos || TextUtils.isEmpty(pkgName)) {
            return;
        }

        myPkgName = pkgName;
        
        // http://www.indelible.org/ink/android-linkify, 09/04/2010
        // La rilevazione dei collegamenti personalizzati e' attiva solo
        // disabilitando la rilevazione dei collegamenti di default e 
        // riabilitandola attraverso la classe Linkify.
        view.setAutoLinkMask(0);
        
        Linkify.addLinks(view, Linkify.ALL);  
        text = String.valueOf(view.getText().subSequence(myStartPos, myEndPos));
        Linkify.addLinks(view, Pattern.compile(text), MarketTagHandler.SCHEME,
                this, this);
    }
    
    /**
     * Restituisce l&rsquo;URL di un collegamento rilevato.
     * 
     * @param  match Corrispondenza rilevata.
     * @param  url   Stringa rilevata.
     * @return       URL del collegamento.
     */
    public String transformUrl(Matcher match, String url) {
        return myPkgName;
    }

    /**
     * Verifica se ad una corrispondenza deve essere applicato un collegamento.
     * 
     * @param  s     Testo.
     * @param  start Indice del primo carattere della corrispondenza.
     * @param  end   Indice del carattere successivo alla corrispondenza.
     * @return       Esito della verifica.
     */
    public boolean acceptMatch(CharSequence s, int start, int end) {
        return (start == myStartPos && end == myEndPos);
    }    
}
