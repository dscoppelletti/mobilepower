/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.content.*;
import android.os.*;
import android.text.*;

/**
 * Dialogo con barra di avanzamento.
 * 
 * <P>Mentre &egrave; aperto un dialogo con barra di avanzamento, le voci di
 * men&ugrave; e della barra delle azioni non sono attivabili.</P>
 * 
 * @since 1.0
 */
public final class ProgressDialogFragment extends AbstractDialogFragment {
    
    /**
     * Tag del dialogo.
     */
    public static final String TAG = "ProgressDialog";
    
    private static final String ARG_TITLEID = "titleId";
    private static final String ARG_MAX = "max";
    private static final String ARG_CANCELABLE = "cancelable";
    private DialogInterface.OnCancelListener myOnCancelListener;
    
    /**
     * Costruttore.
     */
    public ProgressDialogFragment() {        
    }    
    
    /**
     * Istanzia un frammento.
     * 
     * @param  titleId    Identificatore della risorsa del titolo.
     * @param  max        Valore massimo dell&rsquo;indice di avanzamento. Se
     *                    &egrave; minore o uguale a {@code 0}, l&rsquo;indice
     *                    di avanzamento &egrave; indeterminato.
     * @param  cancelable Indicatore di dialogo cancellabile.
     * @return            Frammento.
     */    
    public static ProgressDialogFragment newInstance(int titleId, int max,
            boolean cancelable) {
        Bundle args;
        ProgressDialogFragment fragment;
        
        args = new Bundle();
        args.putInt(ProgressDialogFragment.ARG_TITLEID, titleId);
        args.putInt(ProgressDialogFragment.ARG_MAX, max);
        args.putBoolean(ProgressDialogFragment.ARG_CANCELABLE, cancelable);
        
        fragment = new ProgressDialogFragment();
        fragment.setArguments(args);
        fragment.setCancelable(cancelable);
        
        return fragment;         
    }
        
    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int max, resId;
        ProgressDialog dlg;
        Bundle args = getArguments();
        
        dlg = new ProgressDialog(getActivity(), AppUtils.getDialogTheme());
        
        resId = args.getInt(ProgressDialogFragment.ARG_TITLEID);
        dlg.setTitle(getString(resId));
        max = args.getInt(ProgressDialogFragment.ARG_MAX, -1);
        
        if (max > 0) {
            dlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dlg.setIndeterminate(false);
            dlg.setMax(max);
        } else {
            dlg.setIndeterminate(true);
            dlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        }

        dlg.setCancelable(args.getBoolean(
                ProgressDialogFragment.ARG_CANCELABLE));
        dlg.setCanceledOnTouchOutside(false);

        return dlg;
    }    
    
    /**
     * Imposta la percentuale di avanzamento.
     * 
     * @param value Percentuale.
     */
    private void setProgress(int value) {
        ProgressDialog dlg = (ProgressDialog) getDialog();
     
        dlg.setProgress(value);
    }    
    
    /**
     * Imposta il gestore dell&rsquo;annullamento.
     * 
     * @param obj Oggetto.
     */
    public void setOnCancelListener(DialogInterface.OnCancelListener obj) {
        myOnCancelListener = obj;
    }
    
    /**
     * Gestisce l&rsquo;annullamento.
     * 
     * @param dlg Dialogo.
     */
    @Override
    public void onCancel(DialogInterface dlg) {
        super.onCancel(dlg);
        
        if (myOnCancelListener != null) {
            myOnCancelListener.onCancel(dlg);
        }
    }
    
    /**
     * Chiude un dialogo.
     * 
     * @param fragmentMgr Gestore dei frammenti.
     * @param tag         Tag del frammento.
     */    
    public static void dismiss(FragmentManager fragmentMgr, String tag) {
        ProgressDialogFragment dlg;
        
        if (TextUtils.isEmpty(tag)) {
            throw new NullPointerException("Argument tag is null.");
        }        
        if (fragmentMgr == null) {
            return;
        }
        
        dlg = (ProgressDialogFragment) fragmentMgr.findFragmentByTag(tag);                
        if (dlg != null) {
            dlg.dismiss();
        }        
    }
    
    /**
     * Imposta la percentuale di avanzamento esposta da un dialogo.
     * 
     * @param fragmentMgr Gestore dei frammenti.
     * @param tag         Tag del frammento.
     * @param progress    Percentuale di avanzamento.
     */    
    public static void setProgress(FragmentManager fragmentMgr, String tag,
            int progress) {
        ProgressDialogFragment dlg;
        
        if (TextUtils.isEmpty(tag)) {
            throw new NullPointerException("Argument tag is null.");
        }        
        if (fragmentMgr == null) {
            return;
        }
        
        dlg = (ProgressDialogFragment) fragmentMgr.findFragmentByTag(tag);                
        if (dlg != null) {
            dlg.setProgress(progress);
        }        
    }    
}
