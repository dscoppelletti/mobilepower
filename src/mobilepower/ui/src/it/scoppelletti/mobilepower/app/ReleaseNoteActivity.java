/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app;

import android.app.*;
import android.os.*;
import android.text.*;
import android.text.util.*;
import android.view.*;
import android.widget.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Attivit&grave; di visualizzazione delle note di rilascio
 * dell&rsquo;applicazione.
 * 
 * @since 1.0
 */
public final class ReleaseNoteActivity extends Activity {

    /**
     * Azione di visualizzazione delle note di rilascio dell&rsquo;applicazione.
     */
    public static final String ACTION_RELEASENOTES =
            "it.scoppelletti.mobilepower.intent.action.RELEASENOTES";
           
    /**
     * Parametro <CODE>{@value}</CODE>: Note di rilascio.
     */
    public static final String EXTRA_RELEASENOTES = "releasenotes";
        
    /**
     * Costruttore.
     */
    public ReleaseNoteActivity() {
    }
    
    /**
     * Creazione dell&rsquo;attivit&agrave;.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza.
     */    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String notes;
        TextView textControl;
        Button button;
        MarketTagHandler tagHandler;        
        
        super.onCreate(savedInstanceState);
        
        setTheme(AppUtils.getActivityTheme());
        setContentView(R.layout.releasenotes);
                
        textControl = (TextView) findViewById(R.id.txt_content);
        textControl.setKeyListener(null);
        textControl.setAutoLinkMask(Linkify.EMAIL_ADDRESSES |
                Linkify.WEB_URLS);
        
        notes = getIntent().getStringExtra(
                ReleaseNoteActivity.EXTRA_RELEASENOTES);
        if (!TextUtils.isEmpty(notes)) {
            tagHandler = new MarketTagHandler();
            textControl.setText(Html.fromHtml(notes, null, tagHandler));
            tagHandler.addLinks(textControl,
                    AppUtils.getFullPackageName(this, false));
        }
        
        button = (Button) findViewById(R.id.cmd_continue);
        button.setOnClickListener(new View.OnClickListener() {            
            public void onClick(View v) {
                finish();
            }
        });
    }        
}
