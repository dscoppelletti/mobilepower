/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app.bluetooth;

import android.app.*;
import android.bluetooth.*;
import android.content.*;
import it.scoppelletti.mobilepower.app.*;
import it.scoppelletti.mobilepower.bluetooth.*;

/**
 * Gestore dei dispositivi Bluetooth.
 * 
 * @since 1.0
 */
public final class ActivityBTManager extends BTManager { 
    
    /**
     * Costruttore.
     * 
     * @param activity Attivit&agrave;
     */
    public ActivityBTManager(Activity activity) {
        super(activity);
    }
    
    /**
     * Rilascia le risorse.
     */
    public void onDestroy() {
        detachAdapter();
    }
    
    /**
     * Ripristino dell&rsquo;attivit&agrave;.
     */
    public void onResume() {
        // Registra il gestore degli eventi BroadcastReceiver
    }
    
    /**
     * Pausa dell&rsquo;attivit&agrave;.
     */
    public void onPause() {
        // Deregistra il gestore degli eventi BroadcastReceiver
    }
        
    /**
     * Gestisce il risultato di un&rsquo;attivit&agrave;.
     * 
     * @param requestCode Codice della richiesta.
     * @param resultCode  Risultato dell&rsquo;attivit&agrave;.
     * @param data        Dati trasmessi dall&rsquo;attivit&agrave;.
     */    
    public void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        switch (requestCode) {
        case ActivitySupport.REQ_BTENABLE:
            if (resultCode == Activity.RESULT_OK) {
                onRun();
            } else {
                super.onDisabled();
            }            
            break;
        }
    }
    
    @Override
    protected void onDisabled() {
        Intent intent;
        Activity activity = (Activity) getContext();
        
        intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);        
        activity.startActivityForResult(intent, ActivitySupport.REQ_BTENABLE);
    }
}
