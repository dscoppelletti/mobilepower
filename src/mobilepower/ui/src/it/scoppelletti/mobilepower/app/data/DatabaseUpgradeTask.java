/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app.data;

import android.database.sqlite.*;
import android.os.*;
import org.slf4j.*;
import it.scoppelletti.mobilepower.data.*;
import it.scoppelletti.mobilepower.os.*;

/**
 * Aggiornamento del database.
 */
final class DatabaseUpgradeTask extends AbstractAsyncTask {
    
    /**
     * Parametro {@code current}: Versione corrente.
     */
    static final String PARAM_CURRENTVERSION = "current";
    
    /**
     * Parametro {@code target}: Versione alla quale aggiornare.
     */
    static final String PARAM_TARGETVERSION = "target";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            DatabaseUpgradeTask.class);         
    private final SQLiteDatabase myDB;
    private final DatabaseUpgrader myUpgrader;
    
    /**
     * Costruttore.
     * 
     * @param db        Database.
     * @param upgrader  Procedura di aggiornamento.
     * @param stepCount Numero di passi del processo.
     * @param host      Ospite. Pu&ograve; essere {@code null}. 
     */
    DatabaseUpgradeTask(SQLiteDatabase db, DatabaseUpgrader upgrader,
            int stepCount, AsyncTaskHost host) {
        super(0, host);
        
        if (db == null) {
            throw new NullPointerException("Argument db is null.");
        }
        if (upgrader == null) {
            throw new NullPointerException("Argument upgrader is null.");
        }
        
        myDB = db;
        myUpgrader = upgrader;
        setStepCount(stepCount);
    }    
    
    protected Bundle start(Bundle params) {
        int targetVer, ver;
        
        ver = params.getInt(DatabaseUpgradeTask.PARAM_CURRENTVERSION, 0);
        targetVer = params.getInt(DatabaseUpgradeTask.PARAM_TARGETVERSION, 0);

        while (ver < targetVer) {            
            if (isCancelled()) {
                return params;
            }
            
            ver++;
            myLogger.debug("Upgrading database from version {} to version {}.",
                    ver - 1, ver);
            myUpgrader.onDatabaseUpgrade(myDB, ver);
            myDB.setVersion(ver);
            incrementProgress();
        }        
        
        return params;
    }      
}
