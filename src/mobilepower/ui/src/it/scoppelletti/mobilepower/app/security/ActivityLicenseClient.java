/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app.security;

import android.app.*;
import android.content.*;
import android.os.*;
import com.google.android.vending.licensing.*;
import org.slf4j.*;
import it.scoppelletti.mobilepower.app.*;
import it.scoppelletti.mobilepower.security.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Client del servizio di verifica delle licenze.
 * 
 * @since 1.0
 */
public final class ActivityLicenseClient extends LicenseClient implements
        Runnable, Handler.Callback, DialogInterface.OnCancelListener {
    
    /**
     * Tag dei dialoghi.
     */
    public static final String DIALOG_TAG = "LicenseDialog";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            ActivityLicenseClient.class); 
    private final ActivitySupport myActivity;
    private final Handler myHandler;
    
    /**
     * Costruttore.
     * 
     * @param activity       Attivit&agrave;.
     * @param licenseChecker Client del servizio di verifica delle licenze.
     */    
    public ActivityLicenseClient(Activity activity,
            LicenseChecker licenseChecker) {
        super(licenseChecker);
        
        if (activity instanceof ActivitySupport) {
            myActivity = (ActivitySupport) activity;
        } else {
            myActivity = null;
            myLogger.warn(
                    "Activity not implement interface ActivitySupport.");
        }
        
        myHandler = new Handler(this);
    }

    /**
     * Schedula la verifica della licenza.
     */
    @Override
    public void check() {
        myHandler.post(this);
    }
    
    /**
     * Esegue la verifica della licenza.
     */    
    public void run() {       
        ProgressDialogFragment dlg;
        FragmentManager fragmentMgr;
        
        if (myActivity != null) {
            fragmentMgr = myActivity.asActivity().getFragmentManager();
            dlg = ProgressDialogFragment.newInstance(
                    R.string.lbl_licenseChecking, 0, true);
            dlg.setOnCancelListener(this);            
            dlg.show(fragmentMgr, ActivityLicenseClient.DIALOG_TAG); 
        }
        
        super.check();        
    }
    
    @Override
    protected void onLicenseResult(int result) {
        myHandler.sendEmptyMessage(result);
    }

    /**
     * Gestisce un messaggio.
     * 
     * @param  msg Messaggio.
     * @return     Indica se il messaggio &egrave; stato gestito.
     */
    public boolean handleMessage(Message msg) {
        LicenseBuyDialogFragment buyDlg;
        LicenseRetryDialogFragment retryDlg;
        FragmentManager fragmentMgr = null;
        
        if (myActivity != null) {
            if (myActivity.asActivity().isFinishing()) {
                // L'attivita' sta comunque terminando
                return true;
            }
        
            fragmentMgr = myActivity.asActivity().getFragmentManager(); 
            ProgressDialogFragment.dismiss(fragmentMgr,
                    ActivityLicenseClient.DIALOG_TAG);
        }
        
        switch (msg.what) {
        case LicenseClient.RESULT_POSITIVE:
            onDestroy();
            if (myActivity != null) {
                myActivity.onNextAsyncInitializer();
            }            
            break;
            
        case LicenseClient.RESULT_RETRY:
            if (fragmentMgr == null) {
                throw new RuntimeException("Application not licensed.");
            }
            
            retryDlg = LicenseRetryDialogFragment.newInstance();
            retryDlg.setOperation(this);
            retryDlg.show(fragmentMgr, ActivityLicenseClient.DIALOG_TAG);
            break;            
            
        default: // LicenseClient.RESULT_NEGATIVE:
            if (fragmentMgr == null) {
                throw new RuntimeException("Application not licensed.");
            }
            
            buyDlg = LicenseBuyDialogFragment.newInstance();
            buyDlg.show(fragmentMgr, ActivityLicenseClient.DIALOG_TAG);
            break;                        
        }
                              
        return true;
    }
    
    /**
     * Gestisce l&rsquo;interruzione della verifica della licenza. 
     */
    public void onCancel(DialogInterface dialog) {
        if (myActivity != null) {
            myActivity.asActivity().finish();
        }
    }    
}
