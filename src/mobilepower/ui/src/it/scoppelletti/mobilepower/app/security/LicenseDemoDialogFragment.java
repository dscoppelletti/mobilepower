/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app.security;

import android.app.*;
import android.content.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import it.scoppelletti.mobilepower.app.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Dialogo che segnala l&rsquo;utilizzo di un&rsquo;applicazione di demo.
 * 
 * @since 1.0
 */
public final class LicenseDemoDialogFragment extends AbstractDialogFragment
    implements DialogInterface.OnClickListener {
    private static final String ARG_MSG = "msg";
    
    /**
     * Costruttore.
     */
    public LicenseDemoDialogFragment() {        
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @return Frammento.
     */
    public static LicenseDemoDialogFragment newInstance() {
        return LicenseDemoDialogFragment.newInstance(R.string.msg_notLicensed);
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @param  msgId Identificatore del messaggio.
     * @return       Frammento.
     */
    public static LicenseDemoDialogFragment newInstance(int msgId) {
        Bundle args;
        LicenseDemoDialogFragment fragment;
        
        args = new Bundle();
        args.putInt(LicenseDemoDialogFragment.ARG_MSG, msgId);
        
        fragment = new LicenseDemoDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int msgId;
        View contentView;
        AlertDialog dlg;
        AlertDialog.Builder builder;        
        LayoutInflater inflater;
        TextView textControl;
        Bundle args = getArguments();
        
        builder = newAlertDialogBuilder();
        builder.setTitle(getActivity().getTitle());
        inflater = getActivity().getLayoutInflater();
        contentView = inflater.inflate(R.layout.helpdialog, null);
        builder.setView(contentView);        
        
        builder.setPositiveButton(R.string.cmd_buy, this);
        builder.setNegativeButton(android.R.string.cancel, null);
        
        dlg = builder.create();
        
        textControl = (TextView) contentView.findViewById(R.id.txt_help);
        textControl.setKeyListener(null);
        
        msgId = args.getInt(LicenseDemoDialogFragment.ARG_MSG,
                R.string.msg_notLicensed);
        if (msgId > 0) {
            textControl.setText(Html.fromHtml(getResources().getString(msgId)));
        }
        
        return dlg;
    }

    /**
     * Gestisce la chiusura del dialogo.
     * 
     * @param dialog Dialogo.
     * @param which  Identificatore del bottone.
     */
    public void onClick(DialogInterface dialog, int which) {
        Intent intent;
        Activity activity;
        
        switch (which) {
        case DialogInterface.BUTTON_POSITIVE:
            activity = getActivity();
            intent = AppUtils.newBuyIntent(activity);
            activity.startActivity(intent);
            activity.finish();
            break;
        }
    }    
}
