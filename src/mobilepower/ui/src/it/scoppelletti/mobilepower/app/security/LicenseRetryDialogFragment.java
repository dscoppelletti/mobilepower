/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.app.security;

import android.app.*;
import android.content.*;
import android.os.*;
import it.scoppelletti.mobilepower.app.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Dialogo per ritentare la verifica di una licenza.
 * 
 * @since 1.0
 */
public final class LicenseRetryDialogFragment extends AbstractDialogFragment
    implements DialogInterface.OnClickListener {
    private Runnable myOperation;
    
    /**
     * Costruttore.
     */
    public LicenseRetryDialogFragment() {        
    }
    
    /**
     * Istanzia un frammento.
     * 
     * @return Frammento.
     */
    public static LicenseRetryDialogFragment newInstance() {
        return new LicenseRetryDialogFragment();
    }
    
    /**
     * Imposta l&rsquo;operazione.
     * 
     * @param obj Oggetto.
     */
    public void setOperation(Runnable obj) {
        myOperation = obj;
    }
    
    /**
     * Crea il dialogo.
     * 
     * @param  savedInstanceState Stato dell&rsquo;istanza.
     * @return                    Dialogo.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder;
        
        builder = newAlertDialogBuilder();
        builder.setTitle(getActivity().getTitle());
        builder.setMessage(R.string.msg_licenseRetry);
        builder.setPositiveButton(R.string.cmd_retry, this);
        builder.setNegativeButton(R.string.cmd_exit, this);
        
        return builder.create();
    }

    /**
     * Gestisce la chiusura del dialogo.
     * 
     * @param dialog Dialogo.
     * @param which  Identificatore del bottone.
     */
    public void onClick(DialogInterface dialog, int which) {
        Activity activity;
        
        switch (which) {
        case DialogInterface.BUTTON_POSITIVE:
            if (myOperation != null) {
                myOperation.run();
            }
            break;
            
        case DialogInterface.BUTTON_NEGATIVE:
            activity = getActivity();
            activity.finish();
            break;
        }
    }
    
    /**
     * Gestisce l&rsquo;annullamento.
     * 
     * @param dlg Dialogo.
     */
    @Override
    public void onCancel(DialogInterface dlg) {
        Activity activity = getActivity();
        
        super.onCancel(dlg);        
        activity.finish();
    }
}
