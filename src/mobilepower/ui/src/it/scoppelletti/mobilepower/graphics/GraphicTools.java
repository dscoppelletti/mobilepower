/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.graphics;

import android.graphics.*;

/**
 * Funzioni di utilit&agrave; grafica.
 * 
 * @since 1.0
 */
public final class GraphicTools {

    /**
     * Costruttore privato per classe statica.
     */
    private GraphicTools() {        
    }
    
    /**
     * Imposta la dimensione di campionamento per la decodifica di
     * un&rsquo;immagine in funzione della dimensione di visualizzazione
     * dell&rsquo;immagine stessa.
     * 
     * @param opts       Opzioni di decodifica dell&rsquo;immagine tra le quali
     *                   deve essere gi&agrave; impostata la dimensione
     *                   originale dell&rsquo;immagine
     *                   {@code (outWidth, outHeight)} e sulla quale sar&agrave;
     *                   impostata la dimensione di campionamento
     *                   {@code inSampleSize}.  
     * @param viewWidth  Larghezza di visualizzazione dell&rsquo;immagine.
     * @param viewHeight Altezza di visualizzazione dell&rsquo;immagine.
     */
    public static void setSampleSize(BitmapFactory.Options opts, int viewWidth,
            int viewHeight) {
        int size, size2;
        float scale, scaleX, scaleY;

        if (opts == null) {
            throw new NullPointerException("Argument opts is null.");
        }
        if (opts.outWidth <= 0 || opts.outHeight <= 0) {
            throw new IllegalArgumentException(
                    "Original size is not properly determinated.");
        }
        if (viewWidth <= 0) {
            throw new IllegalArgumentException(
                    "Argument viewWidth must be > 0.");            
        }
        if (viewHeight <= 0) {
            throw new IllegalArgumentException(
                    "Argument viewWidth must be > 0.");            
        }
        
        opts.inSampleSize = 1;
        if (opts.outWidth <= viewWidth && opts.outHeight <= viewHeight) {
            return;
        }
        
        scaleX = ((float) opts.outWidth) / ((float) viewWidth);
        scaleY = ((float) opts.outWidth) / ((float) viewWidth);
        scale = (scaleX < scaleY) ? scaleX : scaleY;
        if (scale <= 1.0) {
            return;            
        }
        
        // La decodifica e' piu' veloce e' efficiente se la dimensione di
        // campionamento e' una potenza di 2        
        size2 = Math.round(Math.getExponent(scale));
        if (size2 <= 1) {
            return;                        
        }
        
        size = 1;
        size <<= size2;
        opts.inSampleSize = size;
    }
}
