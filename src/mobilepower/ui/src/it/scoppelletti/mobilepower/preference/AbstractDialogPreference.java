/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import android.content.*;
import android.os.*;
import android.preference.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.slf4j.*;

/**
 * Classe di base dei dialoghi di impostazione dei parametri di configurazione.
 * 
 * @since 1.0
 */
public abstract class AbstractDialogPreference extends DialogPreference {
    private static final Logger myLogger = LoggerFactory.getLogger(
            AbstractDialogPreference.class);

    /**
     * Costruttore.
     * 
     * @param context Contesto.
     * @param attrs   Attributi.
     */
    protected AbstractDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);        
    }    
        
    /**
     * Crea la vista. 
     * 
     * @param  parent Vista parent.
     * @return        Vista.
     */
    @Override
    protected View onCreateView(ViewGroup parent) {
        View view, child;
        TextView summaryControl;
        Spannable.Factory spannableFactory;
        
        summaryControl = null;
        view = super.onCreateView(parent);
        
        child = view.findViewById(android.R.id.summary);
        if (child == null) {
            myLogger.warn("Control {} not found.", android.R.id.summary);
            return view;            
        }
        if (!(child instanceof TextView)) {
            myLogger.warn("Control {} is an instance of class {}.",
                    android.R.id.summary, child.getClass().getName());
            return view;
        }
        
        summaryControl = (TextView) child;        
        spannableFactory = newSummarySpannableFactory(summaryControl);
        if (spannableFactory == null) {
            return view;
        }
                
        summaryControl.setText("", TextView.BufferType.SPANNABLE);        
        summaryControl.setSpannableFactory(spannableFactory);
        
        return view;
    }
    
    /**
     * Istanzia il compositore del testo del sommario.
     * 
     * @param  summaryControl Controllo del sommario.
     * @return                Oggetto. Pu&ograve; essere {@code null}.
     */
    protected Spannable.Factory newSummarySpannableFactory(
            TextView summaryControl) {
        return null;
    }
    
    /**
     * Chiusura del dialogo.
     * 
     * @param positiveResult Indicatore di conferma del dialogo.
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            notifyChanged();
        }
    }
    
    /**
     * Ripristina lo stato dell&rsquo;istanza.
     * 
     * @param state Stato.
     */
    @Override
    protected final void onRestoreInstanceState(Parcelable state) {
        Bundle data;
        PreferenceSavedState savedState;
        
        if (!(state instanceof PreferenceSavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        
        savedState = (PreferenceSavedState) state;
        
        super.onRestoreInstanceState(savedState.getSuperState());
        data = savedState.getData();                        
        
        if (data != null) {
            onRestoreInstanceState(data);
        }        
    }    
    
    /**
     * Ripristina lo stato dell&rsquo;istanza.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza.
     */
    protected abstract void onRestoreInstanceState(Bundle savedInstanceState);
    
    /**
     * Registra lo stato dell&rsquo;istanza.
     * 
     * @return Stato.
     */
    @Override
    protected final Parcelable onSaveInstanceState() {
        Bundle data;
        Parcelable source;
        PreferenceSavedState state;
        
        source = super.onSaveInstanceState();
        if (isPersistent()) {
            return source;
        }
        
        data = new Bundle();
        onSaveInstanceState(data);
        
        source = super.onSaveInstanceState();
        state = new PreferenceSavedState(source);
        state.setData(data);
        
        return state;
    }
    
    /**
     * Salva lo stato dell&rsquo;istanza.
     * 
     * @param outState Stato.
     */
    protected abstract void onSaveInstanceState(Bundle outState);        
}
