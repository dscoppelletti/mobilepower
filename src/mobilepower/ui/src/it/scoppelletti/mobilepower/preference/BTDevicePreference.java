/*
 * Copyright (C) 2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import java.util.*;
import android.bluetooth.*;
import android.content.Context;
import android.preference.ListPreference;
import android.text.*;
import android.util.AttributeSet;
import android.view.*;
import it.scoppelletti.mobilepower.bluetooth.*;
import it.scoppelletti.mobilepower.types.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Selezione di un dispositivo Bluetooth.
 * 
 * @since 1.0
 */
public final class BTDevicePreference extends ListPreference implements BTTask,
    OnBTListener {
    private BTManager myBTManager;
    
    /**
     * Costruttore.
     * 
     * @param ctx   Contesto.
     * @param attrs Attributi.
     */
    public BTDevicePreference(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        
        myBTManager = new BTManager(ctx);
        myBTManager.setOnBTListener(this);
        myBTManager.run(this);
    }

    /**
     * Collega la preferenza alla vista. 
     * 
     * @param view Vista.
     */
    @Override
    protected void onBindView(View view) {
        setSummary();        
        super.onBindView(view);        
    }
    
    /**
     * Crea il dialogo. 
     * 
     * @param view Dialogo.
     */
    @Override
    protected View onCreateDialogView() {
        // L'utente potrebbe aver abilitato o disabilitato il Bluetooth dopo
        // aver aperto l'attivita' di configurazione:
        // Rileggo i dispositivi accoppiati.
        myBTManager.run(this);
        
        return super.onCreateDialogView();
    }
    
    /**
     * Chiusura del dialogo.
     * 
     * @param positiveResult Indicatore di conferma del dialogo.
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        
        if (positiveResult) {
            setSummary();
        }                
    }
    
    /**
     * Imposta il sommario.
     */
    private void setSummary() {
        CharSequence entry;
        
        entry = getEntry();
        if (TextUtils.isEmpty(entry)) {
            setSummary(getContext().getString(R.string.lbl_noDeviceSelected));
        } else {
            setSummary(entry);
        }
    }

    public void run(BluetoothAdapter adapter) {
        int i, n;
        CharSequence[] entries, entryValues;
        Set<BluetoothDevice> devices;
        
        devices = adapter.getBondedDevices();
        n = devices.size();
        if (n > 0) {
            entries = new CharSequence[n];
            entryValues = new CharSequence[n];
            i = 0;
            for (BluetoothDevice device : devices) {
                entries[i] = BTManager.getName(device);
                entryValues[i] = device.getAddress();
                i++;
            }                
        } else {
            entries = new CharSequence[1];
            entryValues = new CharSequence[1];
            
            entries[0] = getContext().getText(R.string.lbl_BTNoDevicePaired);
            entryValues[0] = StringTools.EMPTY;
        }
        
        setEntries(entries);
        setEntryValues(entryValues);
    }        
    
    public void onNotSupported() {
        CharSequence[] entries, entryValues;
        
        entries = new CharSequence[1];
        entryValues = new CharSequence[1];
        
        entries[0] = getContext().getText(R.string.lbl_BTNotSupported);
        entryValues[0] = StringTools.EMPTY;       
        
        setEntries(entries);
        setEntryValues(entryValues);            
    }

    public void onDisabled() {
        CharSequence[] entries, entryValues;
        
        entries = new CharSequence[1];
        entryValues = new CharSequence[1];
        
        entries[0] = getContext().getText(R.string.lbl_BTDisabled);
        entryValues[0] = StringTools.EMPTY;       
        
        setEntries(entries);
        setEntryValues(entryValues);            
    }       
}
