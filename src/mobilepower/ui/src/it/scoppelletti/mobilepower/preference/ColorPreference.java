/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.xml.sax.*;
import it.scoppelletti.mobilepower.ui.resources.R;
import it.scoppelletti.mobilepower.widget.*;

/**
 * Selezione di un colore.
 * 
 * @since 1.0
 */
public final class ColorPreference extends AbstractDialogPreference {
    
    /**
     * Tag {@code red}: Componente rosso del codice RGB del colore.
     */
    public static final String TAG_RED = "red";
    
    /**
     * Tag {@code green}: Componente verde del codice RGB del colore.
     */
    public static final String TAG_GREEN = "green";
    
    /**
     * Tag {@code blue}: Componente blu del codice RGB del colore.
     */
    public static final String TAG_BLUE = "blue";
    
    private static final String STATE_VALUE = "value";
    private int myValue;
    private int myEditingValue;
    private ImageView myEditingPreview;
    
    /**
     * Costruttore.
     * 
     * @param context Contesto.
     * @param attrs   Attributi.
     */
    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        setDialogLayoutResource(R.layout.colorpreference);        
    }       

    /**
     * Restituisce il valore.
     * 
     * @return Valore.
     */
    private int getValue() {
        return myValue;
    }
    
    /**
     * Imposta il colore.
     * 
     * @param value     Valore.
     * @param component Codice del componente.
     */
    private void setColor(int value, int component) {
        int red, green, blue;
        
        switch (component) {
        case Color.RED:
            red = value;
            green = Color.green(myEditingValue);
            blue = Color.blue(myEditingValue);           
            myEditingValue = Color.rgb(red, green, blue);
            break;
            
        case Color.GREEN:
            red = Color.red(myEditingValue);
            green = value;
            blue = Color.blue(myEditingValue);
            myEditingValue = Color.rgb(red, green, blue);
            break;
            
        case Color.BLUE:
            red = Color.red(myEditingValue);
            green = Color.green(myEditingValue);
            blue = value;
            myEditingValue = Color.rgb(red, green, blue);
            break;
            
        default:
            myEditingValue = value;
            break;
        }
                 
        myEditingPreview.setImageDrawable(new ColorDrawable(myEditingValue));
    }
        
    /**
     * Restituisce il valore di default impostato via XML.
     * 
     * @param  attrs Attributi impostati.
     * @param  index Indice del valore di default.
     * @return       Valore di default.
     */
    @Override
    protected Object onGetDefaultValue(TypedArray attrs, int index) {
        return attrs.getInteger(index, Color.WHITE);                
    }    
    
    /**
     * Imposta il valore iniziale della preferenza.
     * 
     * @param restorePersistedValue Indicatore di valore gi&agrave; registrato.
     * @param defaultValue          Valore di default.
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue,
            Object defaultValue) {
        if (restorePersistedValue) {
            myValue = getPersistedInt(Color.WHITE);
        } else {
            myValue = (Integer) defaultValue;            
            persistInt(myValue);
        }               
    }
   
    @Override
    protected Spannable.Factory newSummarySpannableFactory(
            TextView summaryControl) {
        return new ColorPreference.SummarySpannableFactory(this,
                (int) summaryControl.getTextSize());        
    }
    
    /**
     * Collega la preferenza alla vista. 
     * 
     * @param view Vista.
     */
    @Override
    protected void onBindView(View view) {
        if (TextUtils.isEmpty(getSummary())) {
            setSummary(R.string.msg_colorpreference);
        }
        
        super.onBindView(view);        
    }
    
    /**
     * Collega la preferenza al dialogo. 
     * 
     * @param view Dialogo.
     */
    @Override
    protected void onBindDialogView(View view) {
        IntegerSpinner component;
        
        myEditingPreview = (ImageView) view.findViewById(R.id.img_preview);
        setColor(myValue, Color.WHITE);
        
        component = (IntegerSpinner) view.findViewById(R.id.txt_red); 
        component.setValue(Color.red(myValue));
        component.setOnValueChangedListener(
                new ColorPreference.OnComponentChangedListener(this,
                        Color.RED));
        
        component = (IntegerSpinner) view.findViewById(R.id.txt_green); 
        component.setValue(Color.green(myValue));
        component.setOnValueChangedListener(
                new ColorPreference.OnComponentChangedListener(this,
                        Color.GREEN));
        
        component = (IntegerSpinner) view.findViewById(R.id.txt_blue); 
        component.setValue(Color.blue(myValue));
        component.setOnValueChangedListener(
                new ColorPreference.OnComponentChangedListener(this,
                        Color.BLUE));        
    }
    
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        
        if (positiveResult) {
            myValue = myEditingValue;
            persistInt(myValue);                    
        }
    }
    
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        myValue = savedInstanceState.getInt(ColorPreference.STATE_VALUE,
                Color.WHITE);
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ColorPreference.STATE_VALUE, myValue);
    }
    
    /**
     * Gestore della modifica di un componente.
     */
    private static final class OnComponentChangedListener implements
            IntegerSpinner.OnValueChangedListener {
        private final ColorPreference myControl;
        private final int myComponent;
        
        /**
         * Costruttore 
         * 
         * @param control   Controllo.
         * @param component Codice del componente.
         */
        OnComponentChangedListener(ColorPreference control, int component) {
            myControl = control;
            myComponent = component;
        }
        
        public void onValueChanged(int newValue, int prevValue) {
            myControl.setColor(newValue, myComponent);
        }        
    }
    
    /**
     * Compositore del testo del sommario.
     */
    private static final class SummarySpannableFactory extends
            Spannable.Factory implements Html.ImageGetter, Html.TagHandler {
        private final ColorPreference myControl;
        private final int myTextSize;
        
        /**
         * Costruttore.
         * 
         * @param control  Controllo.
         * @param textSize Dimensioni del testo.
         */
        SummarySpannableFactory(ColorPreference control, int textSize) {
            myControl = control;
            myTextSize = textSize;
        }
        
        /**
         * Costruisce il testo corrispondente ad una stringa.
         * 
         * @param  source Stringa sorgente.
         * @return        Testo risultante.
         */
        @Override
        public Spannable newSpannable(CharSequence source) {
            Spanned s;

            s = Html.fromHtml(String.valueOf(source), this, this);
            
            return SpannableString.valueOf(s);
        }

        /**
         * Restituisce il disegno corrispondente ad un&rsquo;immagine.
         * 
         * @param  source Percorso dell&rsquo;immagine.
         * @return        Disegno.
         */
        public Drawable getDrawable(String source) {
            PaintDrawable color;

            if (!TextUtils.isEmpty(source)) {
                return null;
            }
                 
            color = new PaintDrawable(myControl.getValue());
            color.setBounds(0, 0, myTextSize, myTextSize);
            return color;
        }

        /**
         * Gestisce un tag.
         * 
         * @param opening   Indicatore di tag aperto.
         * @param tag       Tag.
         * @param output    Testo di output.
         * @param xmlReader Flusso di lettura.
         */
        public void handleTag(boolean opening, String tag, Editable output,
                XMLReader xmlReader) {
            int color;
            
            if (!opening) {
                return;
            }

            if (ColorPreference.TAG_RED.equalsIgnoreCase(tag)) {
                color = Color.red(myControl.getValue());
            } else if (ColorPreference.TAG_GREEN.equalsIgnoreCase(tag)) {
                color = Color.green(myControl.getValue());
            } else if (ColorPreference.TAG_BLUE.equalsIgnoreCase(tag)) {
                color = Color.blue(myControl.getValue());
            } else {
                return;
            }
            
            output.append(String.valueOf(color));
        }        
    }
}
