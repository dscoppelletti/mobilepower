/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import android.content.*;
import android.preference.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.slf4j.*;
import org.xml.sax.*;

/**
 * Preferenza rappresentata da un testo.
 * 
 * @since 1.0
 */
public final class EditTextPreferenceEx extends EditTextPreference {
    
    /**
     * Tag {@code text}: Valore.
     */
    public static final String TAG_TEXT = "text";
    
    private static final Logger myLogger = LoggerFactory.getLogger(
            EditTextPreferenceEx.class);
    
    /**
     * Costruttore.
     * 
     * @param context Contesto.
     * @param attrs   Attributi.
     */
    public EditTextPreferenceEx(Context context, AttributeSet attrs) {
        super(context, attrs);         
    }
    
    /**
     * Crea la vista. 
     * 
     * @param  parent Vista parent.
     * @return        Vista.
     */
    @Override
    protected View onCreateView(ViewGroup parent) {
        View view, child;
        TextView summaryControl;
        Spannable.Factory spannableFactory;
        
        summaryControl = null;
        view = super.onCreateView(parent);
        
        child = view.findViewById(android.R.id.summary);
        if (child == null) {
            myLogger.warn("Control {} not found.", android.R.id.summary);
            return view;            
        }
        if (!(child instanceof TextView)) {
            myLogger.warn("Control {} is an instance of class {}.",
                    android.R.id.summary, child.getClass().getName());
            return view;
        }
        
        summaryControl = (TextView) child;        
        spannableFactory =
                new EditTextPreferenceEx.SummarySpannableFactory(this);       
        summaryControl.setText("", TextView.BufferType.SPANNABLE);        
        summaryControl.setSpannableFactory(spannableFactory);
        
        return view;
    }
    
    /**
     * Chiusura del dialogo.
     * 
     * @param positiveResult Indicatore di conferma del dialogo.
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            notifyChanged();
        }
        
        super.onDialogClosed(positiveResult);
    }
    
    /**
     * Compositore del testo del sommario.
     */
    private static final class SummarySpannableFactory extends
            Spannable.Factory implements Html.TagHandler {
        private final EditTextPreferenceEx myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        SummarySpannableFactory(EditTextPreferenceEx control) {
            myControl = control;
        }
        
        /**
         * Costruisce il testo corrispondente ad una stringa.
         * 
         * @param  source Stringa sorgente.
         * @return        Testo risultante.
         */
        @Override
        public Spannable newSpannable(CharSequence source) {
            Spanned s;

            s = Html.fromHtml(String.valueOf(source), null, this);
            
            return SpannableString.valueOf(s);
        }

        /**
         * Gestisce un tag.
         * 
         * @param opening   Indicatore di tag aperto.
         * @param tag       Tag.
         * @param output    Testo di output.
         * @param xmlReader Flusso di lettura.
         */
        public void handleTag(boolean opening, String tag, Editable output,
                XMLReader xmlReader) {
            if (!opening) {
                return;
            }

            if (EditTextPreferenceEx.TAG_TEXT.equalsIgnoreCase(tag)) {
                output.append(String.valueOf(myControl.getText()));
            }
        }        
    }    
}
