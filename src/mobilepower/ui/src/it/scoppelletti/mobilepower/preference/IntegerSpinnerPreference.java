/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import android.content.*;
import android.content.res.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.xml.sax.*;
import it.scoppelletti.mobilepower.ui.resources.R;
import it.scoppelletti.mobilepower.widget.*;

/**
 * Selezione di un valore intero.
 * 
 * @since 1.0
 */
public final class IntegerSpinnerPreference extends AbstractDialogPreference {

    /**
     * Tag {@code value}: Valore.
     */
    public static final String TAG_VALUE = "value";
    
    private static final String STATE_VALUE = "value";
    private static final String ATTR_VALUEMIN = "valueMin";
    private static final String ATTR_VALUEMAX = "valueMax";    
    private static final String ATTR_DELTA = "delta";
    private int myValue;
    private int myValueMin;
    private int myValueMax;    
    private int myDelta;
    private int myEditingValue;
    
    /**
     * Costruttore.
     * 
     * @param context Contesto.
     * @param attrs   Attributi.
     */
    public IntegerSpinnerPreference(Context context, AttributeSet attrs) {        
        super(context, attrs);
        
        int i, n;
        String name;
        
        myValueMin = 0;
        myValueMax = Integer.MAX_VALUE;
        myDelta = 1;
        n = attrs.getAttributeCount();
        for (i = 0; i < n; i++) {            
            name = attrs.getAttributeName(i);
            if (IntegerSpinnerPreference.ATTR_VALUEMIN.equals(name)) {
                myValueMin = attrs.getAttributeIntValue(i, myValueMin); 
            } else if (IntegerSpinnerPreference.ATTR_VALUEMAX.equals(name)) {
                myValueMax = attrs.getAttributeIntValue(i, myValueMax);
            } else if (IntegerSpinnerPreference.ATTR_DELTA.equals(name)) {
                myDelta = attrs.getAttributeIntValue(i, myDelta);
            }
        }
        
        setDialogLayoutResource(R.layout.integerspinnerpreference);        
    }       

    /**
     * Restituisce il valore.
     * 
     * @return Valore.
     */
    private int getValue() {
        return myValue;
    }
      
    /**
     * Imposta il valore in corso di modifica.
     * 
     * @param value     Valore.
     */
    private void setEditingValue(int value) {           
        myEditingValue = value;       
    }
         
    /**
     * Restituisce il valore di default impostato via XML.
     * 
     * @param  attrs Attributi impostati.
     * @param  index Indice del valore di default.
     * @return       Valore di default.
     */
    @Override
    protected Object onGetDefaultValue(TypedArray attrs, int index) {
        return attrs.getInteger(index, 0);                
    }    
    
    /**
     * Imposta il valore iniziale della preferenza.
     * 
     * @param restorePersistedValue Indicatore di valore gi&agrave; registrato.
     * @param defaultValue          Valore di default.
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue,
            Object defaultValue) {
        if (restorePersistedValue) {
            myValue = getPersistedInt(0);
        } else {
            myValue = (Integer) defaultValue;            
            persistInt(myValue);
        }               
    }
    
    @Override
    protected Spannable.Factory newSummarySpannableFactory(
            TextView summaryControl) {
        return new IntegerSpinnerPreference.SummarySpannableFactory(this);        
    }
    
    /**
     * Collega la preferenza al dialogo. 
     * 
     * @param view Dialogo.
     */
    @Override
    protected void onBindDialogView(View view) {
        IntegerSpinner control;
                       
        control = (IntegerSpinner) view;
        control.setValueRange(myValueMin, myValueMax);
        control.setDelta(myDelta);
        control.setValue(myValue);
        myValue = control.getValue();
        setEditingValue(myValue);       
        
        control.setOnValueChangedListener(
                new IntegerSpinnerPreference.OnValueChangedListener(this));
    }   
    
    /**
     * Chiusura del dialogo.
     * 
     * @param positiveResult Indicatore di conferma del dialogo.
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        
        if (positiveResult) {
            myValue = myEditingValue;
            persistInt(myValue);                    
        }
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        myValue = savedInstanceState.getInt(
                IntegerSpinnerPreference.STATE_VALUE, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(IntegerSpinnerPreference.STATE_VALUE, myValue);
    }
    
    /**
     * Gestore della modifica del valore.
     */    
    private static final class OnValueChangedListener implements
        IntegerSpinner.OnValueChangedListener {
        private final IntegerSpinnerPreference myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */        
        OnValueChangedListener(IntegerSpinnerPreference control) {
            myControl = control;
        }

        public void onValueChanged(int newValue, int prevValue) {
            myControl.setEditingValue(newValue);
        }
    }
    
    /**
     * Compositore del testo del sommario.
     */
    private static final class SummarySpannableFactory extends
            Spannable.Factory implements Html.TagHandler {
        private final IntegerSpinnerPreference myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        SummarySpannableFactory(IntegerSpinnerPreference control) {
            myControl = control;
        }
        
        /**
         * Costruisce il testo corrispondente ad una stringa.
         * 
         * @param  source Stringa sorgente.
         * @return        Testo risultante.
         */
        @Override
        public Spannable newSpannable(CharSequence source) {
            Spanned s;

            s = Html.fromHtml(String.valueOf(source), null, this);
            
            return SpannableString.valueOf(s);
        }

        /**
         * Gestisce un tag.
         * 
         * @param opening   Indicatore di tag aperto.
         * @param tag       Tag.
         * @param output    Testo di output.
         * @param xmlReader Flusso di lettura.
         */
        public void handleTag(boolean opening, String tag, Editable output,
                XMLReader xmlReader) {
            if (!opening) {
                return;
            }

            if (IntegerSpinnerPreference.TAG_VALUE.equalsIgnoreCase(tag)) {
                output.append(String.valueOf(myControl.getValue()));
            }
        }        
    }    
}
