/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.preference;

import android.content.*;
import android.content.res.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.xml.sax.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Selezione di un valore da un cursore.
 * 
 * @since 1.0
 */
public final class SeekBarPreference extends AbstractDialogPreference {
    
    /**
     * Tag {@code value}: Valore.
     */
    public static final String TAG_VALUE = "value";
    
    private static final String STATE_VALUE = "value";
    private static final String ATTR_VALUEMIN = "valueMin";
    private static final String ATTR_VALUEMAX = "valueMax";
    private static final String ATTR_PREVIEW = "preview";
    private int myValue;
    private int myValueMin;
    private int myValueMax;
    private int myEditingValue;
    private String myPreview;    
    private TextView myEditingPreview;
    
    /**
     * Costruttore.
     * 
     * @param context Contesto.
     * @param attrs   Attributi.
     */
    public SeekBarPreference(Context context, AttributeSet attrs) {        
        super(context, attrs);
        
        int i, n;
        String name;
        
        myValueMin = 0;
        myValueMax = 100;
        n = attrs.getAttributeCount();
        for (i = 0; i < n; i++) {            
            name = attrs.getAttributeName(i);
            if (SeekBarPreference.ATTR_VALUEMIN.equals(name)) {
                myValueMin = attrs.getAttributeIntValue(i, myValueMin); 
            } else if (SeekBarPreference.ATTR_VALUEMAX.equals(name)) {
                myValueMax = attrs.getAttributeIntValue(i, myValueMax); 
            } else if (SeekBarPreference.ATTR_PREVIEW.equals(name)) {
                myPreview = attrs.getAttributeValue(i);
            }
        }
        
        if (myValueMax < 1) {
            myValueMax = 100;
        }
        
        setDialogLayoutResource(R.layout.seekbarpreference);        
    }       

    /**
     * Restituisce il valore.
     * 
     * @return Valore.
     */
    private int getValue() {
        return myValue;
    }
      
    /**
     * Restituisce il valore minimo.
     * 
     * @return Valore.
     */
    private int getValueMin() {
        return myValueMin;
    }
    
    /**
     * Restituisce il valore in corso di modifica.
     * 
     * @return Valore.
     */
    private int getEditingValue() {
        return myEditingValue;
    }
    
    /**
     * Imposta il valore in corso di modifica.
     * 
     * @param value     Valore.
     */
    private void setEditingValue(int value) {           
        myEditingValue = value;
        
        if (TextUtils.isEmpty(myPreview)) {
            myEditingPreview.setText(String.valueOf(myEditingValue));
        } else {
            myEditingPreview.setText(myPreview);            
        }
    }
         
    /**
     * Restituisce il valore di default impostato via XML.
     * 
     * @param  attrs Attributi impostati.
     * @param  index Indice del valore di default.
     * @return       Valore di default.
     */
    @Override
    protected Object onGetDefaultValue(TypedArray attrs, int index) {
        return attrs.getInteger(index, 0);                
    }    
    
    /**
     * Imposta il valore iniziale della preferenza.
     * 
     * @param restorePersistedValue Indicatore di valore gi&agrave; registrato.
     * @param defaultValue          Valore di default.
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue,
            Object defaultValue) {
        if (restorePersistedValue) {
            myValue = getPersistedInt(0);
        } else {
            myValue = (Integer) defaultValue;            
            persistInt(myValue);
        }               
    }

    @Override
    protected Spannable.Factory newSummarySpannableFactory(
            TextView summaryControl) {
        return new SeekBarPreference.SummarySpannableFactory(this);        
    }
    
    /**
     * Collega la preferenza al dialogo. 
     * 
     * @param view Dialogo.
     */
    @Override
    protected void onBindDialogView(View view) {
        SeekBar control;
        
        if (myValue > myValueMax) {
            myValue = myValueMax;
        }
        
        myEditingPreview = (TextView) view.findViewById(R.id.txt_value);
        if (!TextUtils.isEmpty(myPreview)) {
            myEditingPreview.setText("", TextView.BufferType.SPANNABLE);
            myEditingPreview.setSpannableFactory(
                    new SeekBarPreference.PreviewSpannableFactory(this));
        }
        
        setEditingValue(myValue);
        
        control = (SeekBar) view.findViewById(R.id.bar_value);
        control.setMax(myValueMax - myValueMin);        
        control.setProgress(myValue - myValueMin);   
        control.setOnSeekBarChangeListener(
                new SeekBarPreference.OnValueChangeListener(this));
    }
    
    /**
     * Chiusura del dialogo.
     * 
     * @param positiveResult Indicatore di conferma del dialogo.
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        
        if (positiveResult) {
            myValue = myEditingValue;
            persistInt(myValue);                    
        }
    }
    
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        myValue = savedInstanceState.getInt(SeekBarPreference.STATE_VALUE, 0);
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SeekBarPreference.STATE_VALUE, myValue);
    }
    
    /**
     * Gestore della modifica del valore.
     */
    private static final class OnValueChangeListener implements
            SeekBar.OnSeekBarChangeListener {
        private final SeekBarPreference myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        OnValueChangeListener(SeekBarPreference control) {
            myControl = control;
        }
        
        /**
         * Gestisce la modifica del valore.
         * 
         * @param seekBar  Controllo.
         * @param progress Valore.
         * @param fromUser Indicatore di modifica apportata dall&rsquo;utente.
         */
        public void onProgressChanged(SeekBar seekBar,
                int progress, boolean fromUser) {
            myControl.setEditingValue(myControl.getValueMin() + progress);
        }

        /**
         * Gestisce l&rsquo;inizio della modifica del valore.
         * 
         * @param seekBar Controllo.
         */
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        /**
         * Gestisce il termine della modifica del valore.
         * 
         * @param seekBar Controllo.
         */
        public void onStopTrackingTouch(SeekBar seekBar) {
        }        
    }    
    
    /**
     * Compositore del testo del sommario.
     */
    private static final class SummarySpannableFactory extends
            Spannable.Factory implements Html.TagHandler {
        private final SeekBarPreference myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        SummarySpannableFactory(SeekBarPreference control) {
            myControl = control;
        }
        
        /**
         * Costruisce il testo corrispondente ad una stringa.
         * 
         * @param  source Stringa sorgente.
         * @return        Testo risultante.
         */
        @Override
        public Spannable newSpannable(CharSequence source) {
            Spanned s;

            s = Html.fromHtml(String.valueOf(source), null, this);
            
            return SpannableString.valueOf(s);
        }

        /**
         * Gestisce un tag.
         * 
         * @param opening   Indicatore di tag aperto.
         * @param tag       Tag.
         * @param output    Testo di output.
         * @param xmlReader Flusso di lettura.
         */
        public void handleTag(boolean opening, String tag, Editable output,
                XMLReader xmlReader) {
            if (!opening) {
                return;
            }

            if (SeekBarPreference.TAG_VALUE.equalsIgnoreCase(tag)) {
                output.append(String.valueOf(myControl.getValue()));
            }
        }        
    }
    
    /**
     * Compositore del testo dell&rsquo;anteprima.
     */
    private static final class PreviewSpannableFactory extends
            Spannable.Factory implements Html.TagHandler {
        private final SeekBarPreference myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        PreviewSpannableFactory(SeekBarPreference control) {
            myControl = control;
        }
        
        /**
         * Costruisce il testo corrispondente ad una stringa.
         * 
         * @param  source Stringa sorgente.
         * @return        Testo risultante.
         */
        @Override
        public Spannable newSpannable(CharSequence source) {
            Spanned s;

            s = Html.fromHtml(String.valueOf(source), null, this);
            
            return SpannableString.valueOf(s);
        }

        /**
         * Gestisce un tag.
         * 
         * @param opening   Indicatore di tag aperto.
         * @param tag       Tag.
         * @param output    Testo di output.
         * @param xmlReader Flusso di lettura.
         */
        public void handleTag(boolean opening, String tag, Editable output,
                XMLReader xmlReader) {
            if (!opening) {
                return;
            }

            if (SeekBarPreference.TAG_VALUE.equalsIgnoreCase(tag)) {
                output.append(String.valueOf(myControl.getEditingValue()));
            }
        }        
    }    
}
