/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.view;

import android.os.*;
import android.view.*;

/**
 * Stato di un componente.
 * 
 * @since 1.0
 */
public final class ViewSavedState extends View.BaseSavedState {
    private Bundle myData;
    
    /**
     * Servizio di creazione delle istanze.
     */
    public static final Parcelable.Creator<ViewSavedState> CREATOR =
            new Parcelable.Creator<ViewSavedState>() {
        
                /**
                 * Legge un&rsquo;istanza.
                 * 
                 * @param  in Flusso di lettura. 
                 * @return    Oggetto.
                 */        
                public ViewSavedState createFromParcel(Parcel in) {
                    return new ViewSavedState(in);
                }
        
                /**
                 * Crea un vettore di istanze.
                 * 
                 * @param  size Dimensione.
                 * @return      Vettore.
                 */            
                public ViewSavedState[] newArray(int size) {
                    return new ViewSavedState[size];
                }
            };    
    
    /** 
     * Costruttore.
     * 
     * @param source Flusso di lettura.
     */
    public ViewSavedState(Parcelable source) {
        super(source);
    }    
    
    /**
     * Costruttore.
     * 
     * @param source Flusso di lettura.
     */
    private ViewSavedState(Parcel source) {
        super(source);
    
        myData = source.readBundle();
    }
    
    /**
     * Restituisce i dati dello stato.
     * 
     * @return Oggetto.
     */
    public Bundle getData() {
        return myData;
    }
    
    /**
     * Imposta i dati dello stato.
     * 
     * @param obj Oggetto.
     */
    public void setData(Bundle obj) {
        myData = obj;
    }
    
    /**
     * Scrive lo stato.
     * 
     * @param out   Flusso di scrittura.
     * @param flags Modalit&agrave;.  
     */    
    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        
        out.writeBundle(myData);
    }
}
