/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.widget;

import android.content.*;
import android.os.*;
import android.util.*;
import android.widget.*;
import it.scoppelletti.mobilepower.view.*;

/**
 * Classe di base dei controlli composti.
 * 
 * @since 1.0
 */
public abstract class CompoundControl extends FrameLayout {

    /**
     * Costruttore.
     * 
     * @param ctx Contesto.
     */
    protected CompoundControl(Context ctx) {
        super(ctx);   
    }
    
    /**
     * Costruttore.
     * 
     * @param ctx   Contesto.
     * @param attrs Attributi.
     */
    protected CompoundControl(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);                        
    }    
    
    /**
     * Salva lo stato del controllo.
     * 
     * @param container Contenitore.
     */
    @Override
    protected final void dispatchSaveInstanceState(
            SparseArray<Parcelable> container) {
        // Salvo lo stato del solo contenitore e disabilito il salvataggio per
        // i controlli contenuti.
        super.dispatchFreezeSelfOnly(container);   
    }
    
    /**
     * Ripristina lo stato del controllo.
     * 
     * @param container Contenitore.
     */
    @Override
    protected final void dispatchRestoreInstanceState(
         SparseArray<Parcelable> container) {
        // Ripristino lo stato del solo contenitore e disabilito il ripristino
        // per i controlli contenuti.        
        super.dispatchThawSelfOnly(container);
    }        
    
    /**
     * Ripristina lo stato dell&rsquo;istanza.
     * 
     * @param state Stato.
     */
    @Override
    protected final void onRestoreInstanceState(Parcelable state) {
        Bundle data;
        ViewSavedState savedState;
        
        if (!(state instanceof ViewSavedState)) {
             super.onRestoreInstanceState(state);
             return;
        }    

        savedState = (ViewSavedState) state;

        super.onRestoreInstanceState(savedState.getSuperState());
        data = savedState.getData();
        
        if (data != null) {
            onRestoreInstanceState(data);
        }
    }    

    /**
     * Ripristina lo stato dell&rsquo;istanza.
     * 
     * @param savedInstanceState Stato dell&rsquo;istanza.
     */
    protected abstract void onRestoreInstanceState(Bundle savedInstanceState);
    
    /**
     * Salva lo stato dell&rsquo;istanza.
     * 
     * @return Stato.
     */
    @Override
    protected final Parcelable onSaveInstanceState() {
        Bundle data;
        Parcelable source;
        ViewSavedState state;

        data = new Bundle();
        onSaveInstanceState(data);
        
        source = super.onSaveInstanceState();
        state = new ViewSavedState(source);
        state.setData(data);
      
        return state;      
    }
    
    /**
     * Salva lo stato dell&rsquo;istanza.
     * 
     * @param outState Stato.
     */
    protected abstract void onSaveInstanceState(Bundle outState);
}
