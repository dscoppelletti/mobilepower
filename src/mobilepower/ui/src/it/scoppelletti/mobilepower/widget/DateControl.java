/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.widget;

import java.text.*;
import android.app.*;
import android.content.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.slf4j.*;
import it.scoppelletti.mobilepower.app.*;
import it.scoppelletti.mobilepower.types.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Controllo per una data.
 * 
 * @since 1.0
 */
public final class DateControl extends CompoundControl {
    private static final String STATE_DIALOGTAG = "dialogTag";
    private static final String STATE_ISEMPTYALLOWED = "isEmptyAllowed";
    private static final String STATE_ISRESETENABLED = "isResetEnabled";
    private static final String STATE_VALUE = "value";
    private static final String STATE_ERROR = "error";
    private static final Logger myLogger = LoggerFactory.getLogger(
            DateControl.class);
    private String myDialogTag;
    private boolean myIsEmptyAllowed;
    private boolean myIsResetEnabled;
    private SimpleDate myValue;
    private EditText myValueControl;    
    
    /**
     * Costruttore.
     * 
     * @param ctx Contesto.
     */
    public DateControl(Context ctx) {
        super(ctx);    
        init(ctx);
    }
    
    /**
     * Costruttore.
     * 
     * @param ctx   Contesto.
     * @param attrs Attributi.
     */
    public DateControl(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);

        int i, n;
        String name;
                        
        myIsEmptyAllowed = true;
        myIsResetEnabled = false;
        
        n = attrs.getAttributeCount();
        for (i = 0; i < n; i++) {
            name = attrs.getAttributeName(i);
            if (DateControl.STATE_DIALOGTAG.equals(name)) {
                myDialogTag = attrs.getAttributeValue(i); 
            } else if (DateControl.STATE_ISEMPTYALLOWED.equals(name)) {
                myIsEmptyAllowed = attrs.getAttributeBooleanValue(i,
                        myIsEmptyAllowed);
            } else if (DateControl.STATE_ISRESETENABLED.equals(name)) {
                myIsResetEnabled = attrs.getAttributeBooleanValue(i,
                        myIsResetEnabled);
            }
        }
        
        init(ctx);
    }   
    
    /**
     * Inizializzazione.
     * 
     * @param ctx Contesto.
     */
    private void init(Context ctx) {
        View view;
        ImageButton cmd;
        LayoutInflater inflater;
        
        inflater = LayoutInflater.from(ctx);
        view = inflater.inflate(R.layout.datecontrol, this, false);
        addView(view);
    
        myValueControl = (EditText) findViewById(R.id.txt_value);
        myValueControl.setKeyListener(null);
        
        cmd = (ImageButton) findViewById(R.id.cmd_set);
        cmd.setOnClickListener(new View.OnClickListener() {            
            public void onClick(View view) {
                onSetClick();
            }
        });       
        
        setValue(SimpleDate.NIL);
        
        cmd = (ImageButton) findViewById(R.id.cmd_reset);
        if (myIsResetEnabled) {
            cmd.setOnClickListener(new View.OnClickListener() {            
                public void onClick(View view) {
                    setValue(SimpleDate.NIL);
                }
            });              
        } else {            
            cmd.setVisibility(View.GONE);
        }       
    }   
    
    /**
     * Restituisce il valore.
     * 
     * @return Valore.
     */
    public SimpleDate getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     * 
     * @param value Valore.
     */
    public void setValue(SimpleDate value) {
        String s;
        DateFormat fmt;
        
        if (!myIsEmptyAllowed && ValueTools.isNullOrEmpty(value)) {
            value = SimpleDate.getToday();
        }
        
        myValue = value;
        if (ValueTools.isNullOrEmpty(myValue)) {
            s = StringTools.EMPTY;
        } else {
            fmt = android.text.format.DateFormat.getDateFormat(getContext());
            s = fmt.format(myValue.toCalendar().getTime());
        }
        
        myValueControl.setText(s);
        myValueControl.setError(null);
    }
    
    /**
     * Imposta il messaggio di errore.
     * 
     * @param value Valore.
     */
    public void setError(CharSequence value) {
        myValueControl.setError(value);
    }
    
    /**
     * Restituisce il tag del dialogo di selezione della data.
     * 
     * @return Valore.
     */
    public String getDialogTag() {
        return myDialogTag;
    }
    
    /**
     * Imposta il tag del dialogo di selezione della data.
     * 
     * @param value Valore.
     */
    public void setDialogTag(String value) {
        myDialogTag = value;
    }
    
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        myDialogTag = savedInstanceState.getString(
                DateControl.STATE_DIALOGTAG);
        myIsEmptyAllowed = savedInstanceState.getBoolean(
                DateControl.STATE_ISEMPTYALLOWED, true);
        myIsResetEnabled = savedInstanceState.getBoolean(
                DateControl.STATE_ISRESETENABLED, false);        
        setValue((SimpleDate) savedInstanceState.getParcelable(
                DateControl.STATE_VALUE));
        myValueControl.setError(savedInstanceState.getCharSequence(
                DateControl.STATE_ERROR));                
    }
    
    protected void onSaveInstanceState(Bundle outState) {
        if (!TextUtils.isEmpty(myDialogTag)) {
            outState.putString(DateControl.STATE_DIALOGTAG, myDialogTag);
        }        
        if (myIsEmptyAllowed) {
            outState.putBoolean(DateControl.STATE_ISEMPTYALLOWED, true);
        }
        if (myIsResetEnabled) {
            outState.putBoolean(DateControl.STATE_ISRESETENABLED, true);
        }        
        if (!ValueTools.isNullOrEmpty(myValue)) {
            outState.putParcelable(DateControl.STATE_VALUE, myValue);
        }
        if (myValueControl.getError() != null) {
            outState.putCharSequence(DateControl.STATE_ERROR,
                    myValueControl.getError());
        }
    }
    
    /**
     * Apre il dialogo di selezione della data.
     */
    private void onSetClick() {
        Context ctx;
        Activity activity;
        DatePickerDialogFragment dlg;
        
        ctx = getContext();
        if (!(ctx instanceof ActivitySupport)) {
            myLogger.error("Context not implement interface ActivitySupport.");
            return;
        }
        if (TextUtils.isEmpty(myDialogTag)) {
            myLogger.error("Property dialogTag is not set.");
            return;            
        }
        
        dlg = DatePickerDialogFragment.newInstance(myValue);
        dlg.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {           
            public void onDateSet(DatePicker view, int year, int month,
                    int day) {
                doDateSet(year, month, day);
            }
        });
        
        activity = (Activity) ctx;     
        dlg.show(activity.getFragmentManager(), myDialogTag);
    }
            
    /**
     * Gestisce la modifica della data.
     * 
     * @param year  Anno.
     * @param month Mese.
     * @parma day   Giorno.
     */
    private void doDateSet(int year, int month, int day) {
        setValue(new SimpleDate(year, month, day));
    }    
}
