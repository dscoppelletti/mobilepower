/*
 * Copyright (C) 2013-2014 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.widget;

import android.content.*;
import android.util.*;
import android.widget.*;

/**
 * Bottone con immagine.
 * 
 * @since 1.0
 */
public final class ImageButtonCompat extends ImageButton {

    /**
     * Costruttore.
     * 
     * @param ctx Contesto.
     */
    public ImageButtonCompat(Context ctx) {
        super(ctx);    
        init();
    }
    
    /**
     * Costruttore.
     * 
     * @param ctx   Contesto.
     * @param attrs Attributi.
     */
    public ImageButtonCompat(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        init();
    }   
    
    /**
     * Inizializzazione.
     */
    private void init() {
        // Con il tema Thema_Holo le immagini dovrebbero avere un colore di
        // primo piano chiaro e non scuro come con il tema Theme, ma non voglio
        // differenziare le immagini per versione dell'SDK; nemmeno l'SDK con
        // livello delle API 17 pubblica l'id. della risorsa di sfondo
        // btn_default_holo_light, quindi devo ripiegare sul classico sfondo
        // btn_default.
        setBackgroundResource(android.R.drawable.btn_default);               
    }
}
