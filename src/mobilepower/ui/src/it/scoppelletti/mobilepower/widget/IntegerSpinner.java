/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.widget;

import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import it.scoppelletti.mobilepower.ui.resources.R;

/**
 * Controllo per un valore intero con bottoni di incremento e decremento.
 * 
 * @since 1.0
 */
public final class IntegerSpinner extends CompoundControl {
    private static final String STATE_VALUEMIN = "valueMin";
    private static final String STATE_VALUEMAX = "valueMax";    
    private static final String STATE_VALUE = "value";
    private static final String STATE_DELTA = "delta";
    private int myValue;
    private int myDelta;
    private EditText myValueControl;
    private IntegerSpinner.OnValueChangedListener myOnValueChangedListener;
    private IntegerSpinner.ValueTextWatcher myValueWatcher;
    
    /**
     * Costruttore.
     * 
     * @param ctx Contesto.
     */
    public IntegerSpinner(Context ctx) {
        super(ctx);
    
        init(ctx);
        myValueWatcher.setValueRange(0, Integer.MAX_VALUE);
        doSetValue(0);
        myDelta = 1;
    }
    
    /**
     * Costruttore.
     * 
     * @param ctx   Contesto.
     * @param attrs Attributi.
     */
    public IntegerSpinner(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        
        int i, n;
        int delta, min, max, value;
        String name;
                               
        init(ctx);
        
        min = 0;
        max = Integer.MAX_VALUE;
        value = 0;
        delta = 1;
        n = attrs.getAttributeCount();
        for (i = 0; i < n; i++) {            
            name = attrs.getAttributeName(i);
            if (IntegerSpinner.STATE_VALUEMIN.equals(name)) {
                min = attrs.getAttributeIntValue(i, min); 
            } else if (IntegerSpinner.STATE_VALUEMAX.equals(name)) {
                max = attrs.getAttributeIntValue(i, max);
            } else if (IntegerSpinner.STATE_VALUE.equals(name)) {
                value = attrs.getAttributeIntValue(i, value);
            } else if (IntegerSpinner.STATE_DELTA.equals(name)) {
                delta = attrs.getAttributeIntValue(i, delta);
            }
        }        
        
        myValueWatcher.setValueRange(min, max);
        if (value < min) {
            value = min;
        } else if (value > max){
            value = max;
        }
        
        doSetValue(value);
        myDelta = (delta > 0) ? delta : 1;
    }   
        
    /**
     * Inizializzazione.
     * 
     * @param ctx Contesto.
     */
    private void init(Context ctx) {
        View view;
        ImageButton cmd;
        LayoutInflater inflater;
        
        inflater = LayoutInflater.from(ctx);
        view = inflater.inflate(R.layout.integerspinner, this, false);
        addView(view);
                
        myValueControl = (EditText) findViewById(R.id.txt_value);
        myValueWatcher = new IntegerSpinner.ValueTextWatcher(this);
        myValueControl.addTextChangedListener(myValueWatcher);
        
        cmd = (ImageButton) findViewById(R.id.cmd_increment);
        cmd.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                onIncrementClick(view);
            }
        });
        
        cmd = (ImageButton) findViewById(R.id.cmd_decrement);
        cmd.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                onDecrementClick(view);
            }
        });        
    }    

    /**
     * Restituisce il valore.
     * 
     * @return Valore.
     */
    public int getValue() {
        return myValue;
    }
    
    /**
     * Imposta il valore.
     * 
     * @param value Valore.
     */
    public void setValue(int value) {
        if (value < myValueWatcher.getValueMin() ||
                value > myValueWatcher.getValueMax()) {
            throw new IllegalArgumentException(String.format(
                    "Argument value is out of range [%1$s, %2$d].",
                    myValueWatcher.getValueMin(),
                    myValueWatcher.getValueMax())); 
        }
        
        doSetValue(value);
    }
    
    /**
     * Imposta il valore.
     * 
     * @param value Valore.
     */    
    private void doSetValue(int value) {
        int prevValue;
                
        myValueWatcher.setProgrammaticallySetInProgress(true);
        
        try {            
            myValueControl.setTextKeepState(Integer.toString(value));
            
            if (value != myValue) {
                prevValue = myValue;
                myValue = value;   
                
                if (myOnValueChangedListener != null) {
                    myOnValueChangedListener.onValueChanged(value, prevValue);
                }
            }
        } finally {
            myValueWatcher.setProgrammaticallySetInProgress(false);
        }        
    }
    
    /**
     * Restituisce il valore minimo.
     * 
     * @return Valore.
     */    
    public int getValueMin() {
        return myValueWatcher.getValueMin();
    }
    
    /**
     * Restituisce il valore massimo.
     * 
     * @return Valore.
     */
    public int getValueMax() {
        return myValueWatcher.getValueMax();
    }
    
    /**
     * Imposta i valori minimo e massimo.
     * 
     * @param min Valore minimo.
     * @param max Valore massimo.
     */
    public void setValueRange(int min, int max) {
        myValueWatcher.setValueRange(min, max);
        
        if (myValue < min) {
            doSetValue(min);
        } else if (myValue > max) {
            doSetValue(max);
        }               
    }
        
    /**
     * Restituisce lo scostamento.
     * 
     * @return Valore.
     */
    public int getDelta() {
        return myDelta;
    }
    
    /**
     * Imposta lo scostamento.
     * 
     * @param value Valore.
     */
    public void setDelta(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException(String.format(
                    "Argument value %1$d is not greater than zero.", value));
        }
        
        myDelta = value;
    }

    /**
     * Imposta il gestore della modifica del valore.
     * 
     * @param obj Oggetto.
     */
    public void setOnValueChangedListener(
            IntegerSpinner.OnValueChangedListener obj) {
        myOnValueChangedListener = obj;
    }
    
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        int min, max, value;
        
        min = savedInstanceState.getInt(IntegerSpinner.STATE_VALUEMIN,
                myValueWatcher.getValueMin());
        max = savedInstanceState.getInt(IntegerSpinner.STATE_VALUEMAX,
                myValueWatcher.getValueMax());
        myValueWatcher.setValueRange(min, max);

        value = savedInstanceState.getInt(IntegerSpinner.STATE_VALUE, myValue);
        if (value < min) {
            value = min;
        } else if (value > max){
            value = max;
        }        
        
        doSetValue(value);
        
        value = savedInstanceState.getInt(IntegerSpinner.STATE_DELTA, myDelta);
        myDelta = (value > 0) ? value : 1;
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(IntegerSpinner.STATE_VALUEMIN,
                myValueWatcher.getValueMin());
        outState.putInt(IntegerSpinner.STATE_VALUEMAX,
                myValueWatcher.getValueMax());  
        outState.putInt(IntegerSpinner.STATE_VALUE, myValue);
        outState.putInt(IntegerSpinner.STATE_DELTA, myDelta);
    }
    
    /**
     * Incrementa il valore.
     * 
     * @param view Controllo.
     */
    private void onIncrementClick(View view) {
        int value;
        
        value = myValue + myDelta;
        
        if (value <= myValueWatcher.getValueMax()) {
            doSetValue(value);            
        }                       
    }
    
    /**
     * Decrementa il valore.
     * 
     * @param view Controllo.
     */
    private void onDecrementClick(View view) {
        int value;
        
        value = myValue - myDelta;
        
        if (value >= myValueWatcher.getValueMin()) {
            doSetValue(value);
        }                       
    }

    /**
     * Gestore della modifica del valore.
     * 
     * @since 1.0.0
     */
    public interface OnValueChangedListener {
    
        /**
         * Gestisce la modifica del valore.
         * 
         * @param newValue  Nuovo valore.
         * @param prevValue Valore precedente.
         */
        void onValueChanged(int newValue, int prevValue);
    }
    
    /**
     * Gestore della modifica del valore.
     */
    private static final class ValueTextWatcher extends IntegerTextWatcher {
        private final IntegerSpinner myControl;
        
        /**
         * Costruttore.
         * 
         * @param control Controllo.
         */
        ValueTextWatcher(IntegerSpinner control) {
            myControl = control;
        }             
        
        void onValueChanged(int value) {
            myControl.doSetValue(value);
        }
    }    
}
