/*
 * Copyright (C) 2013 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.mobilepower.widget;

import android.text.*;

/**
 * Gestore della modifica di una casella di testo per numeri interi.
 */
abstract class IntegerTextWatcher implements TextWatcher {
    private int myValueMin;
    private int myValueMax;
    private boolean mySetInProgress;
    
    /**
     * Costruttore.
     */
    IntegerTextWatcher() {
        myValueMin = 0;
        myValueMax = Integer.MAX_VALUE;
    }
    
    /**
     * Restituisce il valore minimo.
     * 
     * @return Valore.
     */
    final int getValueMin() {
        return myValueMin;
    }
        
    /**
     * Restituisce il valore massimo.
     * 
     * @return Valore.
     */
    final int getValueMax() {
        return myValueMax;
    }
    
    /**
     * Imposta i valori minimo e massimo.
     * 
     * @param min Valore minimo.
     * @param max Valore massimo.
     */
    final void setValueRange(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException(String.format(
                    "Argument min %1$d greater than argument max %2$d.", min,
                    max));
        }
        
        myValueMin = min;
        myValueMax = max;
    }
    
    /**
     * Imposta l&rsquo;indicatore di modifica da programma in corso.
     * 
     * @param value Valore.
     */
    final void setProgrammaticallySetInProgress(boolean value) {
        mySetInProgress = value;
    }
    
    /**
     * Gestisce la modifica del testo.
     * 
     * @param s Testo.
     */
    public final void afterTextChanged(Editable s) {            
        int value;
    
        if (mySetInProgress) {
            return;
        }
        
        if (s.length() == 0) {
            s.append(Integer.toString(myValueMin));
            return;
        }
        
        try {
            value = Integer.parseInt(s.toString());
        } catch (NumberFormatException ex) {
            return;
        }
        
        if (value < myValueMin) {
            s.replace(0, s.length(), Integer.toString(myValueMin));
            return;
        }
        if (value > myValueMax) {
            s.replace(0, s.length(), Integer.toString(myValueMax));
            return;                
        }            
        
        onValueChanged(value);
    }

    /**
     * Gestisce la modifica del valore.
     * 
     * @param value Valore.
     */
    abstract void onValueChanged(int value);
    
    /**
     * Gestisce l&rsquo;avviso di modifica del testo.
     * 
     * @param s     Testo originale.
     * @param start Indice del carattere di inizio della modifica del testo.
     * @param count Numero di caratteri del testo da sostituire.
     * @param after Numero di caratteri della stringa sostituita. 
     */
    public final void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
    }

    /**
     * Gestisce la modifica del testo.
     * 
     * @param s
     * @param start  Indice del carattere di inizio della modifica del
     *               testo.
     * @param before Numero di caratteri sostituiti.
     * @param count  Numero di caratteri della stringa sostituita.
     */
    public final void onTextChanged(CharSequence s, int start, int before,
            int count) {            
    }    
}
